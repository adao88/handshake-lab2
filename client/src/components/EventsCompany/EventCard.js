import React from 'react'

const EventCard = ({name, time, location, date, description, eligibility}) => {

    let newDate = new Date(date)
    let formatedDate = (newDate.getMonth() + 1) + '/' + newDate.getDate() + '/' +  newDate.getFullYear()
    
    return(
        <div className="ui items">
            <div className="item">
                <div className="content">
                    <div className="header">Name: {name}</div>
                    <div className="meta">Location: {location}</div>
                    <div className="meta">Date: {formatedDate}</div>
                    <div className="meta">Time: {time}</div>
                    <div className="meta">Description: {description}</div>
                    <div className="meta">Eligibility: {eligibility}</div>
                </div>
            </div>
        </div>
    )

}

export default EventCard
import React , {Component} from 'react'
import {connect} from 'react-redux'
import EventsList from './EventsList'
import EventForm from './EventForm'
import axios from 'axios'
import NavBar from '../NavBar/NavBar'
import AllEventsList from './AllEventsList'
import cookie from 'react-cookies'
import {getEventsInfo} from '../../actions/eventsCompany'

class EventsCompanyTab extends Component {
    constructor(props){
        super(props)
        this.state = {
            allEvents: [],
            events: [],
            showEventForm: false
        }
    }

    componentDidMount(){

        this.props.getEventsInfo()
        this.setState({
            allEvents: this.props.eventPageInfo.events,
            events: this.props.eventPageInfo.registeredEvents
        })

    }

    componentDidUpdate(prevProps, prevState){
        if(prevProps.eventPageInfo !== this.props.eventPageInfo){
            this.setState({
                events: this.props.eventPageInfo.registeredEvents,
                //filteredEvents: this.props.eventPageInfo.events,
                allEvents: this.props.eventPageInfo.events
            })
        }
    }



    handleHideForm = () => {
        this.setState({
            showEventForm: false
        })
    }

    handleShowForm = () => {
        this.setState({
            showEventForm: true
        })
    }

    subtmitForm = (event) => {
        const token = cookie.load('token')
        axios.defaults.headers.common['authorization'] = token
        
        axios.post('/api/create-event', event)
            .then(response => {
                let newEventsArr = this.state.allEvents.concat(response.data.event)
                    this.setState({
                        allEvents: newEventsArr
                    })
            })
    }

    render(){
        return(
            <div>
                <NavBar/>
                <h1>Company Events Tab</h1>
                <div className="ui section divider"></div>
                <div>
                    <div>
                        <h2>Events Created:</h2>
                        <AllEventsList
                            events={this.state.allEvents}
                        />
                        <button className="ui button primary" onClick={this.handleShowForm}>Create Event</button>
                        </div>
                    <div>
                    <div className="ui section divider"></div>
                        <h2>Registered Events:</h2>
                        <EventsList
                            events={this.state.events}
                        />
                        <EventForm
                            hideEventForm={this.handleHideForm}
                            showEventForm={this.state.showEventForm}
                            postNewEvent={this.subtmitForm}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        eventPageInfo: state.companyEventsPageInfo
    }
}
export default connect(mapStateToProps, {getEventsInfo})(EventsCompanyTab)
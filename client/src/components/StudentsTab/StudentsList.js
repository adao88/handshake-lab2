import React from 'react'
import StudentCard from './StudentCard'

const StudentsList = ({students = []}) => {

    return(
        students.map(student=>
            <StudentCard
                key={student._id}
                id={student._id}
                name={student.name}
                schoolname={student.schoolname}
                skillset={student.skillset}
            />
        )
    )

}

export default StudentsList
import React, {Component} from 'react'
import cookie from 'react-cookies'
import {connect} from 'react-redux'
import NavBar from '../NavBar/NavBar'
import {getStudentPreview} from '../../actions/studentPreview'
import axios from 'axios'
import {Link} from 'react-router-dom';


class StudentProfile extends Component {
    constructor(props){
        super(props)
        this.state = {
                name: '',
                location: '',
                objective: '',
                email: '',
                schoolname: '',
                phone: '',
                skillset: ''
        }
        
    
    }

    componentDidMount(){
        this.props.getStudentPreview(this.props.location.id)
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevProps.studentPreviewInfo !== this.props.studentPreviewInfo){
            this.setState({
                name: this.props.studentPreviewInfo.name,
                location: this.props.studentPreviewInfo.location,
                objective: this.props.studentPreviewInfo.objective,
                email: this.props.studentPreviewInfo.email_profile,
                schoolname: this.props.studentPreviewInfo.schoolname,
                phone: this.props.studentPreviewInfo.phone,
                skillset: this.props.studentPreviewInfo.skillset
            })
        }
    }

    startConversation = () => {

        const token = cookie.load('token')
        
        axios.defaults.headers.common['authorization'] = token

        let user_id = this.props.location.id

        axios.post('/api/start-conversation', {user_id})
            
    }
    
    render(){
        return(
            <div>
                <NavBar/>
                <h1>Student Profile</h1>
                <div className="item">
                    <div className="content">
                        <div className="header">Name: {this.state.name}</div>
                        <div className="meta">Objective: {this.state.objective}</div>
                        <div className="description">Location: {this.state.location}</div>
                        <div className="description">Email: {this.state.email}</div>
                        <div className="description">Phone: {this.state.phone}</div>
                        <div className="extra">School: {this.state.schoolname}</div>
                        <div className="extra">Skillset: {this.state.skillset}</div>
                    </div>
                    <div>
                        <Link to="/messages"><button onClick={this.startConversation}>Say Hi!</button></Link>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        studentPreviewInfo: state.studentPreviewInfo
    }
}

export default connect (mapStateToProps, {getStudentPreview})(StudentProfile)
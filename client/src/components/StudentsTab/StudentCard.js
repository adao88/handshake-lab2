import React  from 'react'
import { Link } from 'react-router-dom';

const StudentCard = ({id, name, schoolname, skillset}) => {


    
    return(
        <div className="ui items">
            <div className="item">
                <div className="content">
                    <div className="header">Name: {name}</div>
                    <div className="meta">School: {schoolname}</div>
                    <div className="description">Skills: {skillset}</div>
                    <Link to={{
                        pathname:'/studentProfile',
                        id: id
                        }}>
                        <button variant="raised">
                            See Profile
                        </button>
                    </Link>
                </div>
            </div>
        </div>
    )

}

export default StudentCard
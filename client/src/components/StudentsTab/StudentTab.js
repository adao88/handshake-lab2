import React, {Component} from 'react'
import {connect} from 'react-redux'
import StudentsList from './StudentsList'
import Filter from './Filter'
import NavBar from '../NavBar/NavBar'
import {getStudentList} from '../../actions/studentsTab'


class StudentTab extends Component {
    constructor(props){
        super(props)
        this.state = {
            nameSearch: '',
            skillSearch: '',
            schoolSearch: '',
            filteredStudents: [],
            students: []
        }
    }

    componentDidMount() {
        //fetch all students here and assign to students & filteredStudents
        this.props.getStudentList()

    }

    componentDidUpdate(prevProps, prevState) {
        if(prevProps.studentPageInfo !== this.props.studentPageInfo) {
            this.setState({
                students: this.props.studentPageInfo,
                filteredStudents: this.props.studentPageInfo
            })
        }
    }

    reset = () => {
        this.setState({
            filteredStudents: this.state.students
        })
    }

    handleNameSearch = (e) => {
        this.setState({
            nameSearch: e.target.value,
            skillSearch: '',
            schoolSearch: ''
        })
        this.filterByName()
    }

    handleSkillSearch = (e) => {
        this.setState({
            skillSearch: e.target.value,
            schoolSearch: '',
            nameSearch: ''
        })
        this.filterBySkill()
    }

    handleSchoolSearch = (e) => {
        this.setState({
            schoolSearch: e.target.value,
            nameSearch: '',
            skillSearch: ''
        })
        this.filterBySchool()
    }

    filterByName = () => {
        let lowerCaseNameSearch = this.state.nameSearch.toLowerCase()
        let students = this.state.students
        //filtered Array
        let filteredStudents = students.filter(student => {
            let lowerCaseName = (!student.name) ? '' : student.name.toLowerCase() 
            return lowerCaseName.includes(lowerCaseNameSearch)
        })

        this.setState({
            filteredStudents: filteredStudents
        })
    }

    filterBySkill = () => {
        let lowerCaseSkillSearch = this.state.skillSearch.toLowerCase()
        let students = this.state.students
        //filtered Array
        let filteredStudents = students.filter(student => {
            let lowerCaseSkills = (!student.skillset) ? '' : student.skillset.toLowerCase() 
            return lowerCaseSkills.includes(lowerCaseSkillSearch)
        })

        this.setState({
            filteredStudents: filteredStudents
        })
    }

    filterBySchool = () => {
        let lowerCaseSchoolSearch = this.state.schoolSearch.toLowerCase()
        let students = this.state.students
        //filtered Array
        let filteredStudents = students.filter(student => {
            let lowerCaseSchool = (!student.schoolname) ? '' : student.schoolname.toLowerCase() 
            return lowerCaseSchool.includes(lowerCaseSchoolSearch)
        })

        this.setState({
            filteredStudents: filteredStudents
        })
    }



    render(){
        return(
            <div>
                <NavBar/>
                <h3>Filter By:</h3>
                <Filter
                    handleNameChange={this.handleNameSearch}
                    handleSchoolChange={this.handleSchoolSearch}
                    handleSkillChange={this.handleSkillSearch}
                    handleReset={this.reset}
                />
                <div className="ui section divider"></div>
                <h2>Students:</h2>
                <StudentsList
                    students={this.state.filteredStudents}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        studentPageInfo: state.studentsPageInfo
    }
}
export default connect(mapStateToProps, {getStudentList})(StudentTab)
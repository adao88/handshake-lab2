import React from 'react'
import SideCard from './SideCard'

const SideCardList = ({conversations = [], handleLoadConversation}) => {

    return(
        conversations.map(conversation =>
            <SideCard
                user={conversation.messages[conversation.messages.length-1].user}
                text={conversation.messages[conversation.messages.length-1].text}
                chat_id={conversation._id}
                key={conversation._id}
                handleLoadConversation={handleLoadConversation}
                user1_name={conversation.user1_name}
                user2_name={conversation.user2_name}
            />
        )
    )

}

export default SideCardList
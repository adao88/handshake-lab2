import React from 'react'
import Text from './Text'
const Conversation = ({messages = []}) => {

    return(
            messages.map(message => 
                <Text
                    user={message.user}
                    text={message.text}
                    key={message._id}
                />
            )
    )
}

export default Conversation
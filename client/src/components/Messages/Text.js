import React from 'react'


const Text = ({user, text}) => {
    return(
        <div>
            {user}: {text}
        </div>
    )
}

export default Text
import React from 'react'
import SideCardList from './SideCardList'

const SideBar = ({conversations = [], handleLoadConversation}) =>{
    
  return(
      <div>
          <h3>Messages</h3>
          <div className="ui section divider"></div>
          <SideCardList
            conversations={conversations}
            handleLoadConversation={handleLoadConversation}
          />
      </div>
    )
}

export default SideBar
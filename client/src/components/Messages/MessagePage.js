import React , {Component}from 'react'
import {connect} from 'react-redux'
import Sidebar from './Sidebar'
import NavBar from '../NavBar/NavBar'
import Conversation from './Conversation'
import MessageForm from './MessageForm'
import {getConversations,loadConversation, sendMessage} from '../../actions/messages'


class MessagePage extends Component {
    constructor(props){
        super(props)

        this.state = {
            conversations: [],
            currentConversation: [],
            chat_id: '',
            showMessageForm: false
        }
    }

    componentDidMount(){
        this.props.getConversations()
    }

    componentDidUpdate(prevProps, prevState){
        if(this.props.messagesPageInfo !== prevProps.messagesPageInfo){
            this.setState({
                conversations: this.props.messagesPageInfo.conversations,
                currentConversation: this.props.messagesPageInfo.currentConversation,
                chat_id: this.props.messagesPageInfo.chat_id
            })
        }
    }

    loadConversation = (chat_id) => {
        this.props.loadConversation(chat_id)
        this.showMessageForm()

    }

    submitMessage = (msg) => {
        this.props.sendMessage(msg)
    }

    showMessageForm= () => {
        this.setState({
            showMessageForm: true
        })
    }

    render(){
        return(
            <div>
                <NavBar/>
                <div className="leftcol">
                    <Sidebar
                        conversations={this.state.conversations}
                        handleLoadConversation={this.loadConversation}
                    />
                </div>
                <div className="conversation-box">
                    <Conversation
                        messages={this.state.currentConversation}
                    />
                </div>
                <div className="ui divider"></div>
                <div>
                    <MessageForm
                        chat_id={this.state.chat_id}
                        showForm={this.state.showMessageForm}
                        handleSubmitMessage={this.submitMessage}
                    />
                </div>
            </div>
        )

    }

}

const mapStateToProps = (state) => {
    return{
        messagesPageInfo: state.messagesPageInfo
    }
}

export default connect(mapStateToProps, {getConversations, loadConversation, sendMessage})(MessagePage)
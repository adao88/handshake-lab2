import React , {useState} from 'react'


const MessageForm = ({handleSubmitMessage, chat_id, showForm}) => {

    const [newMessage, setMessage] = useState('')

    const handleNewMessage = (e) => {
        setMessage(e.target.value)
    }

    const submitMessage = () => {
        
        let msgObj = {
            chat_id,
            text: newMessage
        }
        handleSubmitMessage(msgObj)
        setMessage('')
    }

    let show = showForm ? "message display-block" : "message display-none"

    return(
        <div className={show}>
            <div className="ui form">
                <div className="field">
                    <label>Send Message</label>
                    <textarea rows="4" onChange={handleNewMessage} value={newMessage}></textarea>
                    <button onClick={submitMessage}>Send</button>
                </div>
            </div>
        </div>
    )

}

export default MessageForm
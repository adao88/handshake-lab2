import React from 'react'
import cookie from 'react-cookies'

const SideCard = ({user, text, chat_id, handleLoadConversation, user1_name, user2_name}) => {

    let slicedStr = text.slice(0, 20)

    let name = cookie.load('Name')
    let contact = (name === user1_name) ? user2_name : user1_name 

    const loadConversation = (chat_id) => {
        handleLoadConversation(chat_id)
    }
    return(
        <div role="listitem" className="item" onClick={()=>loadConversation(chat_id)}>
            <div className="ui segment">
                <div className="header">{contact}</div> <br/>
                <div className="extra">{user}:    {slicedStr}</div>
            </div>
        </div>
    )

}

export default SideCard
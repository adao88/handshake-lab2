import React, { Component } from 'react';
import axios from 'axios'
import {Link} from 'react-router-dom';
import {Redirect} from 'react-router'



class Register extends Component {
    constructor(props){
        super(props)
    
        this.state = {
            name: "",
            email: "",
            password: "",
            schoolName: "",
            degree: "",
            message: '',
            registration: false
        }
    
    }

    degreeChangeHandler = (e) => {
        this.setState({
            degree: e.target.value
        })
    }

    nameChangeHandler = (e) => {
        this.setState({
            name: e.target.value
        })
    }

    emailChangeHandler = (e) => {
        this.setState({
            email: e.target.value
        })
        
    }

    passwordChangeHandler = (e) => {
        this.setState({
            password: e.target.value
        })
    }

    schoolNameChangeHandler = (e) => {
        this.setState({
            schoolName: e.target.value
        })
    }

    submitRegistration = (e) => {
        e.preventDefault()
        
        const newUser = {
            email: this.state.email,
            password: this.state.password,
            name: this.state.name,
            schoolName: this.state.schoolName,
            degree: this.state.degree
        }
        axios.defaults.withCredentials = true;
        axios.post('/register', newUser)
            .then(response => {
                this.setState({
                    message: response.data.message,
                    registration: response.data.registration
                })
            })
    }



    render(){
        let redirectVar = null
        if(this.state.registration){
            redirectVar = <Redirect to='/login'/>
        }
        return(
            <div>
                <div>
                    <div><Link to="/">Home</Link></div>
                </div>
                <h1 style={{textAlign:"center"}}>
                    Student Registration
                </h1>
                {redirectVar}
                <div className="ui form">
                    <label>Name</label>
                    <div className="field">
                        <input onChange={this.nameChangeHandler} placeholder="Name" type="text"/>
                    </div>
                    <label>School Name</label>
                    <div className="field">
                        <input onChange={this.schoolNameChangeHandler} placeholder="School Name" type="text"/>
                    </div>
                    <label>Email</label>
                    <div className="field">
                        <input onChange={this.emailChangeHandler} placeholder="Email" type="text"/>
                    </div>
                    <label>Password</label>
                    <div className="field">
                        <input onChange={this.passwordChangeHandler} placeholder="Password" type="text"/>
                    </div>
                    <button type="button" onClick={this.submitRegistration}>Register!</button>
                </div>
                {this.state.message}
            </div>
        )
    }
}

export default Register
import React, { Component } from 'react';
import axios from 'axios'
import {Redirect} from 'react-router'
import cookie from 'react-cookies'
import {Link} from 'react-router-dom';



class Login extends Component {
    constructor(props){
        super(props)
    
        this.state = {
            email: "",
            password: "",
            logged: false,
            message: ''
        }
    
    }

    emailChangeHandler = (e) => {
        this.setState({
            email: e.target.value
        })
        
    }

    passwordChangeHandler = (e) => {
        this.setState({
            password: e.target.value
        })
    }

    submitLogin = (e) => {
        e.preventDefault()
        
        const user = {
            email: this.state.email,
            password: this.state.password,
        }
        console.log('User: ', user)

        axios.post('/login', user)
            .then(response => {
                if(response.data.login === 'Success')
                this.setState({
                    logged: true
                })
                this.setState({
                    message: response.data.message
                })
            })
    }



    render(){
        let redirectVar = null
        if(cookie.load("Student-Logged")){
            redirectVar = <Redirect to='/job-tab'/>
        }
        
        return(
            <div>
                <div>
                    <div><Link to="/"><button>Home</button></Link></div>
                </div>
                <h1 style={{textAlign:"center"}}>
                    Student Login
                </h1>
                {redirectVar}
                <div className="ui form">
                    <label>Email</label>                    
                        <div className="field">
                            <input onChange={this.emailChangeHandler} placeholder="email" type="text"/>
                        </div>
                    <label>Password</label>
                        <div className="field">
                            <input onChange={this.passwordChangeHandler} placeholder="password" type="password"/>
                        </div>
                    </div>
                    <div><button className="ui button" type="button" onClick={this.submitLogin}>Login!</button></div>
                {this.state.message}
            </div>
        )
    }
}

export default Login
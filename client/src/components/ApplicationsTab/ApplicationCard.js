import React from 'react'

const ApplicationCard = ({title, company, status, date}) => {

     
    let newDate = new Date(date)
    let formatedDate = (newDate.getMonth() + 1) + '/' + newDate.getDate() + '/' +  newDate.getFullYear()
    
    return(
        <div className="ui items">
            <div className="item">
                <div className="content">
                    <div className="header">{title}</div>
                    <div className="meta">{company}</div>
                    <div className="meta">{status}</div>
                    <div className="description">{formatedDate}</div>
                </div>
            </div>
        </div>
    )



}

export default ApplicationCard
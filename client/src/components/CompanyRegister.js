import React, { Component } from 'react';
import axios from 'axios'
import {Link} from 'react-router-dom';
import {Redirect} from 'react-router'



class CompanyRegister extends Component {
    constructor(props){
        super(props)
    
        this.state = {
            email: "",
            password: "",
            name: "",
            message: ""
        }
    
    }

    nameChangeHandler = (e) => {
        this.setState({
            name: e.target.value
        })
    }

    emailChangeHandler = (e) => {
        this.setState({
            email: e.target.value
        })
        
    }

    passwordChangeHandler = (e) => {
        this.setState({
            password: e.target.value
        })
    }

    submitRegistration = (e) => {
        e.preventDefault()
        
        const newCompany = {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password
        }
        axios.defaults.withCredentials = true;

        axios.post('/company-register', newCompany)
            .then(response => {
                this.setState({
                    message: response.data.message,
                    registration: response.data.registration
                })
            })
    }



    render(){
        let redirectVar = null
        if(this.state.registration){
            redirectVar = <Redirect to='/company-login'/>
        }
        return(
            <div>
                <div>
                    <div><Link to="/">Home</Link></div>
                </div>
                <h1 style={{textAlign:"center"}}>
                    Company Registration
                </h1>
                {redirectVar}
                <div className="ui form">
                    <label>Name</label>
                    <div className="field">
                        <input onChange={this.nameChangeHandler} placeholder="Name" type="text"/>
                    </div>
                    <label>Email</label>
                    <div className="field">
                        <input onChange={this.emailChangeHandler} placeholder="Email" type="text"/>
                    </div>
                    <label>Password</label>
                    <div className="field">
                        <input onChange={this.passwordChangeHandler} placeholder="Password" type="text"/>
                    </div>
                    <button type="button" onClick={this.submitRegistration}>Register!</button>
                </div>
                {this.state.message}
            </div>
            
        )
    }
}

export default CompanyRegister
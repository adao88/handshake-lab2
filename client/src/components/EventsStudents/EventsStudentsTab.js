import React, {Component} from 'react'
import {connect} from 'react-redux'
import EventsList from './EventsList'
import Filter from './Filter'
import RegisteredEventsList from './RegisteredEventsList'
import NavBar from '../NavBar/NavBar'

import {getEventsInfo} from '../../actions/eventsStudents'

class EventsStudentsTab extends Component {

    constructor(props){
        super(props)

        this.state = {
            events: [],
            filteredEvents: [],
            registeredEvents: [],
            nameSearch: ''
        }
    }

    componentDidMount(){

        this.props.getEventsInfo()

        this.setState({
            events: this.props.eventPageInfo.events,
            filteredEvents: this.props.eventPageInfo.events,
            registeredEvents: this.props.eventPageInfo.registeredEvents,
        })
       
    }

    componentDidUpdate(prevProps, prevState){
        if(prevProps.eventPageInfo !== this.props.eventPageInfo){
            this.setState({
                events: this.props.eventPageInfo.events,
                filteredEvents: this.props.eventPageInfo.events,
                registeredEvents: this.props.eventPageInfo.registeredEvents
            })
        }
    }

    reset = () => {
        this.setState({
            filteredEvents: this.state.events
        })
    }


    handleNameChange = (e) => {
        this.setState({
            nameSearch: e.target.value
        })
        this.filterByname()
    }

    filterByname = () => {
        let lowerCaseNameSearch = this.state.nameSearch.toLowerCase()
        let events = this.state.events
        //filtered Array
        let filteredEvents = events.filter(event => {
            let lowerCaseName = (!event.name) ? '' : event.name.toLowerCase() 
            return lowerCaseName.includes(lowerCaseNameSearch)
        })

        this.setState({
            filteredEvents: filteredEvents
        })
    }

    render(){
        return(
            <div>
                <NavBar/>
                <h3>Filter By:</h3>
                <Filter
                    handleNameChange={this.handleNameChange}
                    handleReset={this.reset}
                />
                <div className="ui section divider"></div>
                <h2>Events:</h2>
                <EventsList
                    events={this.state.filteredEvents}
                />
                <div className="ui section divider"></div>
                <h2>Registered Events:</h2>
                <RegisteredEventsList
                    events={this.props.eventPageInfo.registeredEvents}
                />
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return{
        eventPageInfo: state.studentsEventsPageInfo
    }
}

export default connect(mapStateToProps, {getEventsInfo}) (EventsStudentsTab)
import React , {Component} from 'react'
import axios from 'axios'
import NavBar from '../NavBar/NavBar'

class EventProfile extends Component {
    constructor(props){
        super(props)

        this.state = {
            id: props.location.id,
            co_name: props.location.co_name,
            event_name: props.location.event_name,
            date: props.location.formatedDate,
            time: props.location.time,
            location: props.location.location,
            eligibility: props.location.eligibility,
            description: props.location.description,
            co_id: props.location.co_id,
            event_id: props.location.id,
            message: ''
        }
    }

    handleApply = () => {

        let EventApplication = {

            co_name: this.state.co_name,
            eligibility: this.state.eligibility,
            event_id: this.state.event_id,
            event_name: this.state.event_name,
            co_id: this.state.co_id,
            date: this.state.date,
            time: this.state.time,
            location: this.state.location
            
        }

        axios.post('/api/apply-to-event', EventApplication)
            .then(response => {
                this.setState({
                    message: response.data.message
                })
            })

    }

    render(){
        return(
            <div>
                <NavBar/>
                <h1>Event Profile</h1>
                <div className="item">
                    <div className="content">
                        <div className="header">Comany: {this.state.co_name}</div>
                        <div className="meta">Name: {this.state.event_name}</div>
                        <div className="extra">Description: {this.state.description}</div>
                        <div className="description">Location: {this.state.location}</div>
                        <div className="description">Date: {this.state.date}</div>
                        <div className="description">Eligibility: {this.state.eligibility}</div>
                        <div className="extra">Time: {this.state.time}</div>
                    </div>
                </div>
                <button className="ui button primary" onClick={this.handleApply}>Apply!</button>
                <h3>{this.state.message}</h3>
            </div>
        )
    }

}
export default EventProfile
import React from 'react'

const Job = ({title, date, deadline, location, salary, category, description}) => {
    let newDate = new Date(date)
    let newDeadline = new Date(deadline)
    let formatedDate = (newDate.getMonth() + 1) + '/' + newDate.getDate() + '/' +  newDate.getFullYear()
    let formatedDeadline = (newDeadline.getMonth() + 1) + '/' + newDeadline.getDate() + '/' +  newDeadline.getFullYear()
    return(
        <div className="ui items">
            <div className="item">
                <div className="content">
                    <div className="header">Title: {title}</div>
                    <div className="meta">Category: {category}</div>
                    <div className="description">Description: {description}</div>
                    <div className="description">Location: {location}</div>
                    <div className="description">Posted: {formatedDate}</div>
                    <div className="description">Deadline: {formatedDeadline}</div>
                    <div className="extra">Salary: {salary}</div>
                </div>
            </div>
        </div>
    )


}

export default Job
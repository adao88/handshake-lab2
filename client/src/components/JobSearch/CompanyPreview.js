import React , {Component} from 'react'
import {connect} from 'react-redux'
import {getCompanyPreview} from '../../actions/companyPreview'
class CompanyPreview extends Component {
    constructor(props){
        super(props)

        this.state = {
            co_name: '',
            email: '',
            location: '',
            description: '',
            phone: ''
        }
    }

    componentDidMount(){
        this.props.getCompanyPreview(this.props.location.co_id)

    }

    componentDidUpdate(prevProps, prevState){
        if(prevProps.companyPreviewInfo !== this.props.companyPreviewInfo){
            this.setState({
                co_name: this.props.companyPreviewInfo.name,
                email: this.props.companyPreviewInfo.email_profile,
                location: this.props.companyPreviewInfo.location,
                description: this.props.companyPreviewInfo.description,
                phone: this.props.companyPreviewInfo.phone
            })
        }
    }

    render(){
        return(
            <div>
                <h1>Company Profile</h1>
                <div className="item">
                    <div className="content">
                        <div className="header">Name: {this.state.co_name}</div>
                        <div className="meta">Email: {this.state.email}</div>
                        <div className="extra">Description: {this.state.description}</div>
                        <div className="description">Location: {this.state.location}</div>
                        <div className="description">Phone: {this.state.phone}</div>
                    </div>
                </div>
            </div>
        )
    }
    
} 

const mapStateToProps = (state) => {
    return {
        companyPreviewInfo: state.companyPreviewInfo
    }
}

export default connect (mapStateToProps, {getCompanyPreview})(CompanyPreview)
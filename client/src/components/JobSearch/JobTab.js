import {connect} from 'react-redux'
import React, {Component} from 'react'
import JobList from './JobList'
import Filter from './Filter'
import NavBar from '../NavBar/NavBar'
import cookie from 'react-cookies'
import Sort from './Sort'
import{fetchJobSearchPageInfo} from '../../actions/jobSearchPage'

class JobTab extends Component {
    constructor(props){
        super(props)
        this.state = {
            jobs: [],
            filteredJobs: [],
            locationSearch: '',
            sortedJobs: []
        }
        
    }

    componentDidMount(){

        if(cookie.load("Student-Logged")){
            this.props.fetchJobSearchPageInfo()
            
            this.setState({
                //locationSearch: '',
                jobs: this.props.jobSearchPageInfo.JobsPosted,
                filteredJobs: this.props.jobSearchPageInfo.JobsPosted
                })
        }
    }

    componentDidUpdate(prevProps, prevState){
        if(prevProps.jobSearchPageInfo !== this.props.jobSearchPageInfo){
            this.setState({
                jobs: this.props.jobSearchPageInfo.JobsPosted,
                filteredJobs: this.props.jobSearchPageInfo.JobsPosted
            })
        }

        let jobs = this.props.jobSearchPageInfo.JobsPosted

        let sortedJobs = jobs.sort((a,b) => {
            let locationA = a.location.toUpperCase()
            let locationB = b.location.toUpperCase()
            if(locationA < locationB) {return -1}
            if (locationA > locationB) {return 1}
            return 0
        })
    }

    sortLocationAsc = () => {
        
        let jobs = this.props.jobSearchPageInfo.JobsPosted

        let sortedJobs = jobs.sort((a,b) => {
            let locationA = a.location.toUpperCase()
            let locationB = b.location.toUpperCase()
            if(locationA < locationB) {return -1}
            if (locationA > locationB) {return 1}
            return 0
        })

        this.setState({
            filteredJobs: sortedJobs
        })

    }

    sortLocationDesc = () => {
        
        let jobs = this.props.jobSearchPageInfo.JobsPosted

        let sortedJobs = jobs.sort((a,b) => {
            let locationA = a.location.toUpperCase()
            let locationB = b.location.toUpperCase()
            if(locationA < locationB) {return 1}
            if (locationA > locationB) {return -1}
            return 0
        })

        this.setState({
            filteredJobs: sortedJobs
        })

    }

    sortDateAsc = () => {

        let jobs = this.props.jobSearchPageInfo.JobsPosted

        let newDateJobs = jobs.map(job => {
            let date = new Date(job.date)
            let deadline = new Date(job.deadline)
            return {
                ...job,
                date,
                deadline
            }
        })
        
        let sortedJobs = newDateJobs.sort((a,b) => a.date - b.date)
        
        this.setState({
            filteredJobs: sortedJobs
        })

    }
    sortDateDesc = () => {

        let jobs = this.props.jobSearchPageInfo.JobsPosted

        let newDateJobs = jobs.map(job => {
            let date = new Date(job.date)
            let deadline = new Date(job.deadline)
            return {
                ...job,
                date,
                deadline
            }
        })
        
        let sortedJobs = newDateJobs.sort((a,b) => b.date - a.date)
        this.setState({
            filteredJobs: sortedJobs
        })
    }

    sortDeadlineAsc = () => {

        let jobs = this.props.jobSearchPageInfo.JobsPosted

        let newDateJobs = jobs.map(job => {
            let date = new Date(job.date)
            let deadline = new Date(job.deadline)
            return {
                ...job,
                date,
                deadline
            }
        })
        
        let sortedJobs = newDateJobs.sort((a,b) => a.deadline - b.deadline)
        
        this.setState({
            filteredJobs: sortedJobs
        })


    }

    sortDeadlineDesc = () => {

        let jobs = this.props.jobSearchPageInfo.JobsPosted

        let newDateJobs = jobs.map(job => {
            let date = new Date(job.date)
            let deadline = new Date(job.deadline)
            return {
                ...job,
                date,
                deadline
            }
        })
        
        let sortedJobs = newDateJobs.sort((a,b) => b.deadline - a.deadline)
        
        this.setState({
            filteredJobs: sortedJobs
        })
    }

    
    handleLocationSearch = (e) => {
        this.setState({
            locationSearch: e.target.value
        })
        this.filterByLocation()
    }

    reset = () => {
        this.setState({
            filteredJobs: this.state.jobs
        })
    }


    filterByLocation = () => {
        let lowerCaseLocationSearch = this.state.locationSearch.toLowerCase()
        
        let jobs = this.state.jobs 

        let filteredJobs = jobs.filter(job => {
            let lowerCaseLocation = (!job.location) ? '' : job.location.toLowerCase()
            return lowerCaseLocation.includes(lowerCaseLocationSearch)
        })

        this.setState({
            filteredJobs
        })
    }

    handleCategoryChange = (category)  => {
        this.setState({
            categoryFilter: category
        })
        this.filterCategory(this.state.categoryFilter)
    }

    filterCategory = (category) => {
        
        
        let jobs = this.state.jobs 
        let filteredJobs = jobs.filter(job => {
            return job.category === category
        })

        this.setState({
            filteredJobs
        })
    }

    render(){
        
        if(this.props.jobSearchPageInfo.JobsPosted.length < 1){
            return(
                <div>
                    <NavBar/>
                    <h1>Student's Jobs Tab</h1>
                    <h2>Loading</h2>
                </div>
            )
        }
            return(
                <div>
                    <NavBar/>
                    <h1>Student's Jobs Tab</h1>
                    <div className="ui section divider"></div>
                    <Filter
                        handleLocationChange={this.handleLocationSearch}
                        handleCategoryChange={this.handleCategoryChange}
                        handleReset={this.reset}
                    />
                    <Sort
                        sortLocationAsc={this.sortLocationAsc}
                        sortLocationDesc={this.sortLocationDesc}
                        sortDateAsc={this.sortDateAsc}
                        sortDateDesc={this.sortDateDesc}
                        sortDeadlineAsc={this.sortDeadlineAsc}
                        sortDeadlineDesc={this.sortDeadlineDesc}
                    />
                    <div className="ui section divider"></div>
                    <h3>Jobs: </h3>
                    <JobList
                        jobs={this.state.filteredJobs}
                    />
                </div>
            )
        
    }

}

const mapStatetoProps = (state) => {
    return{
        jobSearchPageInfo: state.jobSearchPageInfo
    }
}

export default connect(mapStatetoProps, {fetchJobSearchPageInfo}) (JobTab)
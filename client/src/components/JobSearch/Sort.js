import React from 'react' 

const Filter =({sortLocationAsc, sortLocationDesc, sortDateAsc, sortDateDesc, sortDeadlineAsc, sortDeadlineDesc})=> {

return(
    <div>
        Sort:
        <button onClick={()=>sortLocationAsc()}>
            Location(Asc)
        </button>
        <button onClick={()=>sortLocationDesc()}>
            Location(Desc)
        </button>
        <button onClick={()=>sortDateAsc()}>
            Date(Asc)
        </button>
        <button onClick={()=>sortDateDesc()}>
            Date(Desc)
        </button>
        <button onClick={()=>sortDeadlineAsc()}>
            Deadline(Asc)
        </button>
        <button onClick={()=>sortDeadlineDesc()}>
            Deadline(Desc)
        </button>
    </div>
)

}

export default Filter
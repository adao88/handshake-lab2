import React from 'react'
import { Link } from 'react-router-dom';

const JobCard = ({job_id, title, company, date, deadline, location, salary, category, co_id, description }) => {

        
        let newDate = new Date(date)
        let formatedDate = (newDate.getMonth() + 1) + '/' + newDate.getDate() + '/' +  newDate.getFullYear()
    

        let newDeadline = new Date(deadline)
        let formatedDeadline = (newDeadline.getMonth() + 1) + '/' + newDeadline.getDate() + '/' +  newDeadline.getFullYear()
    
    return(
        <div className="ui items">
            <div className="item">
                <div className="content">
                    <div className="header">Title: {title}</div>
                    <div className="meta">Company: {company}</div>
                    <div className="meta">Location: {location}</div>
                    <div className="meta">Date: {formatedDate}</div>
                    <div className="meta">Deadline: {formatedDeadline}</div>
                    <div className="description">{category}</div>
                    <Link to={{
                        pathname:'/jobProfile',
                        job_id, title, company, date, deadline, location, salary, category, description, co_id
                        }}>
                        <button variant="raised">
                            See Job Descripton
                        </button>
                    </Link>
                    <Link to={{
                        pathname:'/companyPreview',
                        co_id
                        }}>
                        <button variant="raised">
                            See Company Profile
                        </button>
                    </Link>
                </div>
            </div>
        </div>
    )

}

export default JobCard
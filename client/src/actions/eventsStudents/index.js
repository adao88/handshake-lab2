import axios from 'axios'
import cookie from 'react-cookies'

export const getEventsInfo = () => async dispatch => {

    let events = {}
    
    const token = cookie.load('token')
    axios.defaults.headers.common['authorization'] = token

    //axios get all events
    let allEvents = await axios.get('/api/get-all-events')
    .then(response => {
        console.log('response (all events): ', response.data)
        return response.data.events
        
    })

    events.events = allEvents
    
    //axios get registered events
    let registeredEvents = await axios.get('/api/get-registered-events')
        .then(response => {
            console.log('response (registered events): ', response.data)
            return response.data.events
        })

    events.registeredEvents = registeredEvents
    console.log('Student Events (from action): ', events)
    dispatch({
        type: 'GET_EVENTS_INFO',
        payload: events
    })
}
import axios from 'axios'
import cookie from 'react-cookies'

export const getConversations = () => async dispatch => {

    const token = cookie.load('token')
        
    axios.defaults.headers.common['authorization'] = token

    

    let conversations = await axios.get('/api/get-conversations')
        .then(response => {
            return response.data.conversations
            
        })

    dispatch({
        type: 'GET_CONVERSATIONS',
        payload: conversations
    })
}

export const loadConversation = (chat_id) => async dispatch => {

    const token = cookie.load('token')
        
    axios.defaults.headers.common['authorization'] = token

    let conversation = await axios.post('/api/open-conversation', {chat_id})
        .then(response => { 
            return response.data.conversation
        })

    dispatch({
        type: 'LOAD_CONVERSATION',
        payload: conversation
    })
}

export const sendMessage = (msg) => async dispatch => {

    const token = cookie.load('token')
        
    axios.defaults.headers.common['authorization'] = token

    let conversation = await axios.post('api/send-message', msg)
        .then(response => {
            return response.data.conversation.messages
        })

    dispatch({
        type: 'SEND_MESSAGE',
        payload: conversation //messages array
    })

}
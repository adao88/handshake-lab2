import axios from 'axios'
import cookie from 'react-cookies'

export const getCompanyPreview = (co_id) => async dispatch => {

    const token = cookie.load('token')
        
    axios.defaults.headers.common['authorization'] = token
    console.log('co_id: ', co_id)
    let company = await axios.post('/api/get-company-preview', {co_id})
                    .then(response => {
                        console.log('response: ', response)
                        if(!response.data) return {}
                        return response.data
                        
                    })

    dispatch({
        type: 'GET_COMPANY_PREVIEW',
        payload: company
    })

}
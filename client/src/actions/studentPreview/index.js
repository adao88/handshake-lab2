import axios from 'axios'
import cookie from 'react-cookies'


export const getStudentPreview = (id) => async dispatch => {

    const token = cookie.load('token')
        
    axios.defaults.headers.common['authorization'] = token
    
    let student = await axios.post('/api/get-student-page', {id})
        .then(response => {
            console.log('test response: ', response)
            console.log('Student Preview: ', response.data.student)
            return response.data.student
            
        })

    dispatch({
        type: 'GET_STUDENT_PREVIEW',
        payload: student
    })

}
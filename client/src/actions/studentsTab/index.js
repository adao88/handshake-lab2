import axios from 'axios'
import cookie from 'react-cookies'

export const getStudentList = () => async dispatch => {



    const token = cookie.load('token')
    axios.defaults.headers.common['authorization'] = token


    let students = await axios.get('/api/get-student-list')
            .then(response => {
                console.log('Students data: ', response.data)
                return response.data.Students
            })

    console.log('Student List: ', students)
    dispatch({
        type: 'GET_STUDENTS',
        payload: students
    })


}
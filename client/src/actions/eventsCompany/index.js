import axios from 'axios'
import cookie from 'react-cookies'

export const getEventsInfo = () => async dispatch => {
    
        let events = {}

        const token = cookie.load('token')
        
        axios.defaults.headers.common['authorization'] = token
        
        let registeredEvents = await axios.get('/api/get-companyEvents')
            .then(response => {
                console.log('response: ', response.data)
                return response.data.events
            })

        let allEvents = await axios.get('/api/get-all-company-events')
            .then(response => {
                console.log('response: ', response.data)
                return response.data.events
            })

        events.events = allEvents 
        events.registeredEvents = registeredEvents

        console.log('Company Events (from action): ', events)
        dispatch({
            type: 'GET_COMPANY_EVENTS_INFO',
            payload: events
        })

}
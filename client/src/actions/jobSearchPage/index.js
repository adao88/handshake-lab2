import axios from 'axios'
import cookie from'react-cookies'


export const fetchJobSearchPageInfo = () => async dispatch => {

    const token = cookie.load('token')
    console.log('token: ', token)
    axios.defaults.headers.common['authorization'] = token
    let data = await axios.get('/api/get-job-list')
                    .then(response => {
                        console.log('job search page info results: ', response)
                        return response.data.Jobs
                    })
    dispatch({
        type: 'GET_JOB_SEARCH_PAGE',
        payload: data
    })
}


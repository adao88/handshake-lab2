import React from 'react';
import ReactDOM from 'react-dom';
import {render} from 'react-dom'
import {BrowserRouter} from 'react-router-dom'
import './index.css';
import App from './App';
import{Provider} from 'react-redux'
import{createStore, applyMiddleware, compose} from 'redux'
import thunk from 'redux-thunk'
import reducers from './reducers'

/* eslint-disable no-underscore-dangle */
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducers, composeEnhancers( applyMiddleware(thunk)))
/* eslint-enable */
ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
, document.getElementById('root'));

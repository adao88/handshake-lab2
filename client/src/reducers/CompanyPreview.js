const initialState = {
    Company: {}
}

const companyPreviewReducer = (state = initialState, action) => {

    switch(action.type){

        case 'GET_COMPANY_PREVIEW':
            let companyPayload = action.payload 
            console.log('Company Payload from reducer: ', companyPayload)
            return companyPayload
            
        default:
            return state


    }

}

export default companyPreviewReducer
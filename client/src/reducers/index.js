import {combineReducers} from 'redux'
import {selectedInfoReducer} from './select'
import userInfoReducer from './UserInfo'
import companyInfoReducer from './CompanyInfo'
import jobPostPageReducer from './JobPostPage'
import jobSearchPageReducer from './JobSearchPage'
import studentsEventsTabReducer from './StudentsEventsPage'
import companyEventsTabReducer from './CompanyEventsPage'
import studentsPageReducer from './StudentsTab'
import studentPreviewReducer from './StudentPreview'
import companyPreviewReducer from './CompanyPreview'
import messagesReducer from './Messages'

//import the reducers and export it

export default combineReducers({
    userInfo: userInfoReducer,
    companyInfo: companyInfoReducer,
    selectedInfo: selectedInfoReducer,
    jobPostPageInfo: jobPostPageReducer,
    jobSearchPageInfo: jobSearchPageReducer,
    studentsEventsPageInfo: studentsEventsTabReducer,
    companyEventsPageInfo: companyEventsTabReducer,
    studentsPageInfo: studentsPageReducer,
    studentPreviewInfo: studentPreviewReducer,
    companyPreviewInfo: companyPreviewReducer,
    messagesPageInfo: messagesReducer
})
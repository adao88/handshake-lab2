const initialSate = {
    JobsPosted: [],
    //JobsFiltered: []
}

const JobSearchPageReducer = (state = initialSate, action) => {
    switch(action.type){
        case 'GET_JOB_SEARCH_PAGE':
            let jobsPayload = action.payload
            console.log('Getting Job Search Page Info: ', jobsPayload)
            let JobsPosted = [...jobsPayload]
            return{
                JobsPosted
            }

        default:
            return state
    }
}

export default JobSearchPageReducer
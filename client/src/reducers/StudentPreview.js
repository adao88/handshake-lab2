const initialState = {
    Student: {}
}

const studentPreviewReducer = (state = initialState, action) => {

    switch(action.type){
        case 'GET_STUDENT_PREVIEW':
            let studentPayload = action.payload
            console.log('Student Payload from reducer: ', studentPayload)
            return studentPayload
        default:
            return state
    }
}

export default studentPreviewReducer
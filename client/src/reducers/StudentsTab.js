const initialState = {
    Students: []
}

const studentsPageReducer = (state = initialState, action) => {

    switch(action.type){
        case 'GET_STUDENTS':
            let studentsPayload = action.payload 
            console.log('Students Payload: ', studentsPayload)
            let Students = [...studentsPayload]
            return Students
        default:
            return state
    }


}

export default studentsPageReducer
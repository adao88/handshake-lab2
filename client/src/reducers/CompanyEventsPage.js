const initialState = {
    events: [],
    registeredEvents: []


}

const companyEventsTabReducer = (state = initialState, action) => {
    switch(action.type){
        case 'GET_COMPANY_EVENTS_INFO':
            console.log('Getting Company\'s events Page')
            let eventsPayload = action.payload 
            return {
                ...eventsPayload
            }
        
        default:
            return initialState
    }

}

export default companyEventsTabReducer
const initialState = {
    conversations:[],
    currentConversation: [],
    chat_id: ''
}

const messagesReducer = (state = initialState, action) => {
    switch(action.type){

        case 'GET_CONVERSATIONS':
            let conversations = action.payload
            console.log('Conversations(from reducer): ', conversations)
            let conversationsObj = {
                conversations
            }
            return {
                ...state,
                ...conversationsObj               
            }

        case 'LOAD_CONVERSATION':
            let conversation = action.payload 
            let loadObj = {
                currentConversation: conversation.messages,
                chat_id: conversation._id
            }
            console.log('Conversation obj from reducer: ', loadObj)
            return{
                ...state,
                ...loadObj
            }
        case 'SEND_MESSAGE':
            let messages = action.payload
            let messagesObj = {
                currentConversation: messages
            }
            console.log('Updated Messages: ', messagesObj)
            return{
                ...state,
                ...messagesObj
            }
        default:
            return state
    }
}

export default messagesReducer
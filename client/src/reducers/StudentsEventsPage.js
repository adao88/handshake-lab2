const initialState = {
    events: [],
    registeredEvents: []
}

const studentsEventsTabReducer = (state = initialState, action) => {
    switch (action.type){
        case 'GET_EVENTS_INFO':
            console.log('Getting Students events Page')
            let eventsPostsPayload = action.payload
            return {
                ...eventsPostsPayload
            }
        default: 
            return state
    }
}

export default studentsEventsTabReducer
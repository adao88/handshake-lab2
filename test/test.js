var assert = require("assert");
let chai = require("chai");
let chaiHttp = require("chai-http");
let server=require('../index')
let should = chai.should();
chai.use(chaiHttp);

describe("Jobs", function (){
    it("should get all jobs", done => {
        console.log('getting all jobs')
        chai.request(server)
                .get("/api/get-job-list")
                .end((err, result) => {
                    result.should.have.status(200)
                    console.log ("Result Body:", result.body);
                })
                done()
        })
    
})


describe("Students", function () {
    (it("should get all students", done => {
        console.log('getting all students')
        chai.request(server)
            .get("/api/get-student-list")
            .end((err, result) => {
                result.should.have.status(200)
                console.log ("Result Body:", result.body)
            })
            done()
    }))
})


describe("Company", function () {
    (it("should get company profile", done => {
        console.log('getting company page')
        let id = "5e8accf8750fd19fa24a8477"
        chai.request(server)
            .post("/api/get-company-preview")
            .send({co_id: id})
            .end((err, result) => {
                result.should.have.status(200)
                console.log ("Result Body:", result.body)
            })
            done()
    }))
})


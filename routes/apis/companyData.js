const express = require('express')
//const {db} = require('../../db')
const bodyParser = require('body-parser');
const router = express.Router();
const app = express()
const passport = require('passport')
const checkAuth = passport.authenticate("jwt", { session: false });
const Companies = require('../../Models/CompanyModel')

//const kafka = require('../kafka/client')

app.use(bodyParser.json());

//get Company's profile info
router.get('/get-company-profile', checkAuth, (req, res)=> {
    let id = req.session.userId

    console.log('Current user_id (from api): ', id)
    
    let companyInfo = {
        Basic: {},
        Contact: {}
    }
    /*
    let msg = {
        id,
        path: 'Get-Company-Profile'
    }

    kafka.make_request('company_profile', msg, (error, result) => {
        if(error){
            console.log(error)
            res.send({
                message: 'Error in Kafka request'
            })
        } else if (result.success) {
            res.send({
                ...result.companyInfo
            })
        } else if (!result.success){
            res.status(500).send({
                message: result.message
            })
        }
    })

    /*
    */
    Companies.findById(id).exec()
        .then(company => {
            companyInfo.Basic.name = company.name
            companyInfo.Basic.location = company.location
            companyInfo.Basic.description = company.description
            companyInfo.Contact.email = company.email_profile
            companyInfo.Contact.phone = company.phone
            companyInfo.Id = company.id

            res.send({
                ...companyInfo
            })
        })
        .catch(error => {
            console.log(error)
            res.status(500).send({
                message: 'Failed to get company profile'
            })
        })

    /*
    db.query('SELECT * FROM company WHERE id = ?', [`${id}`], (error, results, fields) => {
        if (error) throw error

        let result = results[0]
        console.log('Company Profile results: ', result)

        companyInfo.Basic.name = result.name
        companyInfo.Basic.location = result.location
        companyInfo.Basic.description = result.description

        companyInfo.Contact.email = result.email_profile
        companyInfo.Contact.phone = result.phone
        companyInfo.Id = result.id

        res.send({
            ...companyInfo
        })

    })
    */

})


//Preview Company's profile
router.post('/get-company-preview',checkAuth, (req,res) => {
    let {co_id} = req.body 
    console.log('inside of company preview route')

    /*
    let msg = {
        co_id,
        path: 'Get-Company-Preview'
    }
    
    kafka.make_request('company_profile', msg, (error, result) => {
        console.log('result: ', result.company)
        if(error){
            console.log(error)
            res.send({
                message: 'Error in Kafka request'
            })
        } else if (result.success) {
            let company = {
                ...result.company
            }
            res.send({
                ...company
            })
        } else if (!result.success){
            res.status(500).send({
                message: result.message
            })
        }
    })
    
    */
    Companies.findById(co_id).exec()
        .then(company => {
            console.log('getting company preview from id: ', co_id)
            console.log('company: ', company)
            res.send({
                ...company
            })
        })
        .catch(error => {
            console.log(error)
            res.status(500).send({
                message: 'Failed to get company profile preview'
            })
        })
    /*
    console.log('inside of company preview route')
    db.query('SELECT * FROM company WHERE id = ?', [`${co_id}`], (error, result, fields) => {
        if (error) throw error
        console.log('getting company preview from id: ', co_id)
        res.send({
            ...result[0]
        })
    })
    */
})


//Change company contact info
router.post('/change-company-contact-info', checkAuth, (req,res) => {
    let id = req.session.userId

    let {email, phone} = req.body
    /*
    let msg = {
        id,
        email,
        phone,
        path: 'Change-Company-Contact-Info'
    }

    kafka.make_request('company_profile', msg, (error, result) => {
        console.log('result: ', result.company)
        if(error){
            console.log(error)
            res.send({
                message: 'Error in Kafka request'
            })
        } else if (result.success) {
            res.send({
                ...req.body
            })
        } else if (!result.success){
            res.status(500).send({
                message: result.message
            })
        }
    })
    */
    Companies.findByIdAndUpdate(id, {email, phone}).exec()
        .then(company => {
            console.log('contact info change data: ', req.body)
            res.send({
                ...req.body
            })
        })
        .catch(error => {
            console.log(error)
            res.status(500).send({
                message: 'Unable to change company contact info'
            })
        })

    /*
    db.query("UPDATE company SET email_profile = ?, phone = ? WHERE id = ?",
    [email, phone, id], 
    (error, result) => {
        if(error) throw error
        console.log('result of updating basic company info in DB: ', result)
    })

    console.log('basic info change data: ', req.body)
    res.send({
        ...req.body
    })
    */

})


//Change company basic info
router.post('/change-company-basic-info', checkAuth, (req, res) => {

    let id = req.session.userId

    let {name, location, description} = req.body

    /*
    let msg = {
        id,
        name,
        location,
        description,
        path: 'Change-Company-Basic-Info'
    }

    kafka.make_request('company_profile', msg, (error, result) => {
        console.log('result: ', result.company)
        if(error){
            console.log(error)
            res.send({
                message: 'Error in Kafka request'
            })
        } else if (result.success) {
            res.send({
                ...req.body
            })
        } else if (!result.success){
            res.status(500).send({
                message: result.message
            })
        }
    })
    */
    
    Companies.findByIdAndUpdate(id, {name, location, description}).exec()
        .then(result => {
            console.log('basic info change data: ', req.body)
            res.send({
                ...req.body
            })
        })
        .catch(error => {
            console.log(error)
            res.status(500).send({
                message: 'Unable to change company basic info'
            })
        })
    /*
    db.query("UPDATE company SET name = ?, location = ?, description = ? WHERE id = ?",
    [name, location, description, id], 
    (error, result) => {
        if(error) throw error
        console.log('result of updating basic company info in DB: ', result)
    })

    console.log('basic info change data: ', req.body)
    res.send({
        ...req.body
    })
    */

})


module.exports = {
    companyApiRouter: router
}
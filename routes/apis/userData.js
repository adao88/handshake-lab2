const express = require('express')
//const {db} = require('../../db')
const bodyParser = require('body-parser');
const router = express.Router();
const app = express()

const passport = require('passport')
const checkAuth = passport.authenticate("jwt", { session: false });

const Students = require('../../Models/StudentModel')

//const kafka = require('../kafka/client')

app.use(bodyParser.json());


//query DB with  user_id
router.get('/get-user-profile/', checkAuth, (req, res) => {

    let id = req.session.userId
    console.log('Current user_id (from api): ', id)
    
    /*
    let msg = {
        student_id: id,
        path: 'Get-Student-Profile'
    }

    kafka.make_request('student_profile', msg, async (err, results) => {
        let kafkaResults = await results
        console.log('Kafka student profile results: ', kafkaResults)
        
        if (err){
            console.log("Inside err");
            res.send({
                status:"error",
                message:"System Error, Try Again."
            })
        }
        if(kafkaResults.success){
            
            res.send({
                ...kafkaResults.userInfo
            })
        } else if(!kafkaResults.success){
            res.send({
                message: 'Failed to get Student Info'
            })
        } 
    })
    */
    
    
    let userInfo = {
        Basic: {},
        Education: [],
        Skillset: {},
        Experience: [],
        Id: ''
    }

    Students.findById(id, (error, student) => {
        if (error) {
            console.log('Error in query')
            res.status(500).send({
                message: 'Error'
            })
        }
        if (student) {
            //console.log('student profile: ', student)

            userInfo.Basic.name = student.name
            userInfo.Basic.birthdate = student.birthdate
            userInfo.Basic.location = student.location
            userInfo.Basic.email = student.email_profile
            userInfo.Basic.phone = student.phone
            userInfo.Basic.objective = student.objective
            userInfo.Basic.degree = student.degree
            userInfo.Skillset.skills = student.skillset
            userInfo.Id = student._id
            userInfo.Education = student.education 
            userInfo.Experience = student.experience

            res.send({
                ...userInfo
            })

        }
    })   
    
})

//change basic info of Student
router.post('/change-basic-info', checkAuth, (req, res) => {
    let id = req.session.userId

    let {birthdate, location, objective, email, phone, degree} = req.body
    console.log('basic info change data: ', req.body)
    /*
    let msg = req.body 
    msg.path = 'Change-Basic-Info'
    msg.student_id = id

    kafka.make_request('student_profile', msg, (err, result) => {
        if(err){
            console.log(err)
            res.send({
                message: 'Error Occurred in kafka request',
                ...req.body
            })
        } else if (result.success){
            res.send({
                ...req.body
            })
        } else if (!result.success){
            res.send({
                message: 'Error Occurred in Mongo Query',
                ...req.body
            })
        }
    })
    */
    
    Students.findByIdAndUpdate(id, {birthdate, location, objective,  phone, degree, email_profile: email },(error, student) => {
        if (error) {
            console.log('error here')
            res.status(500).send({
                message: 'Error with update'
            })
            throw error
        }
        if(student) {
            console.log('Updated Student Info: ', student)
        }
    })
    
    res.send({
        ...req.body
    })
    
})

router.post('/change-skills-info', checkAuth, (req, res) => {
    let id = req.session.userId

    let {skillset} = req.body

    //console.log('skillset body: ', req.body)
    /*
    let msg = {
        student_id: id,
        skillset: skillset,
        path: 'Change-Skillset'
    }

    kafka.make_request('student_profile', msg, (err, result) => {
        console.log('result: ', result)
        if(err){
            console.log(err)
        } else if (result.success){
            console.log('Skillset: ', result.skills)
            res.send({
                skills: result.skills 
            })
        } else if (!result.success){
            res.send({
                message: result.message
            })
        }
    })
    */
    Students.findByIdAndUpdate(id, {skillset: skillset}).exec()
        .then(result => {
            res.send({
                skills: skillset
            })
        })
        .catch(error => {
            console.log(error)
            res.status(500).send({
                message: 'Error updating skillset'
            })
        })
    

})

router.post('/add-experience', checkAuth, (req,res) => {
    //let {company, title, location, dates, description, user_id} = req.body
    let id = req.session.userId
    /*
    let msg = {
        ...req.body,
        id: id,
        path: 'Add-Experience'
    }

    kafka.make_request('student_profile', msg, (err, result) => {
        if(err){
            console.log(err)
            res.send({
                message: 'Error in Kafka request'
            })
        } else if (result.success){
            res.send({
                Experience: result.Experience
            })
        } else if (!result.success){
            res.send({
                message: 'Unable to add experience'
            })
        }
    })
    */
    let { 
        company,
        title,
        location,
        dates,
        description 
    } = req.body

    let newExperience = {
        company,
        title,
        location,
        dates,
        description
    }
    
    
    Students.findByIdAndUpdate(id, {$push: {experience: newExperience}}).exec()
        .then(result => {
            console.log('Result ', result)
            let experienceArr = result.experience
            experienceArr.push(newExperience)
            res.send({
                Experience: experienceArr
            })
        })
        .catch(error => {
            console.log(error)
        })
    
})

router.post('/add-education', checkAuth, (req,res) => {
    let {schoolname, degree, location, dates, gpa} = req.body
    let id = req.session.userId
    /*
    let msg = {
        ...req.body,
        id,
        path: 'Add-Education'
    }

    kafka.make_request('student_profile', msg, (err, result) => {
        if(err){
            console.log(err)
            res.send({
                message: 'Error in Kafka request'
            })
        } else if (result.success){
            res.send({
                Education: result.Education
            })
        } else if (!result.success){
            res.send({
                message: 'Unable to add education'
            })
        }
    })
    */
    let newEducation = {
        schoolname,
        degree,
        location, 
        dates,
        gpa
    }

    Students.findByIdAndUpdate(id, {$push: {education: newEducation}}).exec()
        .then(result => {
            console.log('Result ', result)
            let educationArr = result.education
            educationArr.push(newEducation)
            res.send({
                Education: educationArr
            })
        })
        .catch(error => {
            console.log(error)
        })
    
})

router.post('/edit-experience', checkAuth, (req, res) => {
    //let {company, title, location, dates, description, id} = req.body
    let {id} = req.body
    let user_id = req.session.userId
    console.log('Editing Experience id: ', id)

    /*
    let msg = {
        ...req.body,
        user_id,
        path: 'Edit-Experience'
    }

    kafka.make_request('student_profile', msg, (err, result) => {
        if(err){
            res.send({
                message: 'Error in kafka request'
            })
        } else if (result.success){
            res.send({
                Experience: result.Experience
            })
        } else if (!result.success){
            res.send({
                message: result.message
            })
        }
    })
    */
    
    Students.findOneAndUpdate({
        _id: user_id, "experience._id": id 
    }, 
        {
            $set: {
                "experience.$.company": company,
                "experience.$.title": title,
                "experience.$.location": location,
                "experience.$.dates": dates,
                "experience.$.description": description
            }
    })
    .exec()
    .then(response => {
        Students.findById(user_id, (error, student) => {
            if (error){
                console.log(error)
                res.status(500).send({
                    message: "Failed to get experience"
                })
            }
            if(student){
                console.log('Updated Experience: ', student.experience)
                res.send({
                    Experience: student.experience
                })
            }
        })

    })
    .catch(error => {
        console.log(error)
        res.status(500).send({
            message: "Failed to edit experience"
        })
    })  
})

router.post('/edit-education', checkAuth,  (req, res) => {
    let {schoolname, degree, location, dates, gpa, id} = req.body
    let user_id = req.session.userId
    console.log('Editting education id: ', id)
    /*
    let msg = {
        ...req.body,
        user_id,
        path: 'Edit-Education'
    }

    kafka.make_request('student_profile', msg, (err, result) => {
        if(err){
            res.send({
                message: 'Error in kafka request'
            })
        } else if (result.success){
            res.send({
                Education: result.Eeducation
            })
        } else if (!result.success){
            res.send({
                message: result.message
            })
        }
    })
    */
    Students.findOneAndUpdate({
        _id: user_id, "education._id": id
    }, 
    {
        $set: {
            "education.$.schoolname": schoolname,
            "education.$.degree": degree,
            "education.$.location": location,
            "education.$.dates": dates,
            "education.$.gpa": gpa
        }
    }, (error, result) => {
        if (error){
            console.log(error)
            res.status(500).send({
                message: "Failed to update Education"
            })
        }
        if(result){
            Students.findById(user_id, (error, student) => {
                if(error){
                    console.log(error)
                    res.status(500).send({
                        message: 'Failed to get Education'
                    })
                }
                if(student){
                    console.log('Updated Education: ', student.education)
                    res.send({
                        Education: student.education
                    })
                }
            })
        }
    })
    
})

router.post('/delete-experience', checkAuth, (req, res) => {
    let id = req.body.id //id of experience document
    let user_id = req.session.userId
    /*
    let msg = {
        id,
        user_id,
        path: 'Delete-Experience'
    }

    kafka.make_request('student_profile', msg, (error, result) => {
        if(error){
            res.send({
                message: 'Error in kafka request'
            })
        } else if (result.success){
            res.send({
                Experience: result.Experience
            })
        } else if (!result.success){
            res.send({
                message: result.message
            })
        }
    })
    */
    
    Students.findOneAndUpdate({_id: user_id}, {$pull: {experience: {_id: id}}}, (error, result) => {
        if (error){
            res.status(500).send({
                message: 'Failed to delete experience'
            })
        }
        if(result){
            Students.findById(user_id, (error, student) => {
                if(error){
                    console.log(error)
                    res.status(500).send({
                        message: 'Failed to get student experience'
                    })
                }
                if(student){
                    console.log('Updated Experience: ', student.experience)
                    res.send({
                        Experience: student.experience
                    })
                }
            })
        }
    })
    
    
    /*
    db.query('DELETE FROM experience WHERE id = ?', id, (error, results, fields) => {
        if (error) throw error
        console.log(`Deleting Experience with id: ${id}`)
        
        db.query(`SELECT * FROM experience WHERE user_id = ?`, [`${user_id}`], (error, results, fields) => {
            if(error) throw error
            console.log('Updated Experience List: ', results)
            res.send({
                Experience: results
            })
        }) 
    })
    */
})

router.post('/delete-education', checkAuth, (req, res) => {
    let id = req.body.id
    let user_id = req.session.userId
    /*
    let msg = {
        id,
        user_id,
        path: 'Delete-Education'
    }

    kafka.make_request('student_profile', msg, (error, result) => {
        if(error){
            res.send({
                message: 'Error in kafka request'
            })
        } else if (result.success){
            res.send({
                Education: result.Education
            })
        } else if (!result.success){
            res.send({
                message: result.message
            })
        }
    })
    */
    
    Students.findOneAndUpdate({_id: user_id}, {$pull: {education: {_id: id}}}, (error, result) => {
        if (error){
            console.log(error)
            res.status(500).send({
                message: 'Failed to delete education'
            })
        }
        if(result){
            Students.findById(user_id, (error, student) => {
                if(error){
                    console.log(error)
                    res.status(500).send({
                        message: 'Failed to get student education'
                    })
                }
                if(student){
                    console.log('Updated Education: ', student.education)
                    res.send({
                        Education: student.education
                    })
                }
            })
        }
    })
})

//get All Students
router.get('/get-student-list',checkAuth,(req,res) => {
    /*
    let msg = {
        path: 'Get-Student-List'
    }

    kafka.make_request('student_profile', msg, (error, result) => {
        if(error){
            res.send({
                message: 'Error in kafka request'
            })
        } else if (result.success){
            res.send({
                Students: result.Students
            })
        } else if (!result.success){
            res.send({
                message: result.message
            })
        }
    })
    */
    
    Students.find().exec()
        .then(students => {
            console.log('Results (students): ', students)
            res.send({
                Students: students
            })
        })
        .catch(err => {
            console.log(err)
            res.status(500).send({
                message: "Failed to get student list"
            })
        })
    /*
    db.query('SELECT * FROM users', (error, results, fields) => {
        if (error) throw error
        console.log('Fetched student list: ', results)

        res.send({
            Students: results
        })
    })
    */

})

router.post('/get-student-page', checkAuth, (req, res) => {
    let {id} = req.body
    /*
    let msg = {
        id,
        path: 'Get-Student-Page'
    }
    
    kafka.make_request('student_profile', msg, (error, result) => {
        if(error){
            res.send({
                message: 'Error in kafka request'
            })
        } else if (result.success){
            let student = {
                ...result.Student
            }
            console.log('Student: ', result.Student)
            res.send({
                student
            })
        } else if (!result.success){
            res.send({
                message: result.message
            })
        }
    })
    */
    
    Students.findById(id, (error, student) => {
        if (error){
            console.log(error)
            res.status(500).send({
                message: `Failed to get student page of ${id}`
            })
        }
        if(student){
            console.log(`Got student page of ${id}`)
            res.send({
                student
            })
        }
    })   
    /*
    db.query('SELECT * FROM users WHERE id = ?', [`${id}`], (error, results, fields) => {
        let result = results[0]
        res.send({...result})

    })
    */

})


module.exports = {
    userApiRouter: router
}

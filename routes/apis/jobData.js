const express = require('express')
const {db} = require('../../db')
const bodyParser = require('body-parser');
const router = express.Router();
const app = express()
const passport = require('passport')
const checkAuth = passport.authenticate("jwt", { session: false });
const Students = require('../../Models/StudentModel')
const Companies = require('../../Models/CompanyModel')

//const kafka = require('../kafka/client')

app.use(bodyParser.json());


// Get Company's Jobs
router.get('/get-job-post-page', checkAuth, async (req, res) => {

    
    let jobPageInfo = {
        JobsPosted: [],
        JobsApplied: []
    }
    


    let co_id = req.session.userId
    /*
    let msg = {
        co_id,
        path: 'Get-Company-Job-Posts'
    }
    
    kafka.make_request('jobs', msg, (error, result) => {
        if(error){
            console.log('error')
            
            res.send({
                message: 'Error in kafka request'
            })
            
        } else if (result.success){
            let jobPageInfo = {
                ...result
            }
            delete jobPageInfo["success"]
            console.log('JobPageInfoTest: ', jobPageInfo)
            
            res.send({
                ...jobPageInfo
            })
            
        } else if (!result.success){
            console.log('Unsuccessful results')
            
            res.send({
                message: result.message
            })
            
        }
    })
    */
    
    //gets All Jobs posted by company
    await Companies.findById(co_id).exec()
        .then(company => {
            console.log('Company jobs:', company.jobs)
            jobPageInfo.JobsPosted = company.jobs
        })
        .catch(error => {
            console.log(error)
            res.status(500).send({
                message: 'Failed to find Jobs Posted'
            })
        })

    //gets All Comapny's jobs that have been applied to
    await Students.find({
        jobs: {
            $elemMatch: {co_id: co_id}
        }
    }).exec()
        .then(students => {
            //console.log('Company Jobs Applied: ', results)
            //jobPageInfo.JobsApplied = results
            //let appsArr = students.map(student => student.jobs.filter(job => job.co_id === co_id))
            students.forEach(student => student.jobs.forEach(job => {
                if (job.co_id === co_id) jobPageInfo.JobsApplied.push(job)
            }))
            console.log('Company Jobs Applied Array: ', jobPageInfo.JobsApplied)
            
        })
        .catch(error => {
            console.log(error)
            res.status(500).send({
                message: 'Failed to find applied Jobs'
            })
        })

    console.log('Job Info to be sent: ', jobPageInfo)
    res.send({
        ...jobPageInfo
    })
    

})

//Company posting new Job
router.post('/post-new-job', checkAuth, async (req, res) => {

    
    let co_id = req.session.userId
    let co_name = req.session.company_name
    
    let {title, deadline, date, location, salary, description, category} = req.body

    let newJob = {title, deadline, date, location, salary, description, category, co_id, co_name}
    
    /*
    let msg = {
        co_id,
        co_name,
        ...req.body,
        path: 'Post-New-Job'
    }

    kafka.make_request('jobs', msg, (error, result) => {
        if(error){
            console.log('error')
            res.send({
                message: 'Error in kafka request'
            })
        } else if (result.success){
            console.log('Result: ', result.Jobs)
            res.send({
                JobsPosted: result.Jobs
            })
        } else if (!result.success){
            console.log('Unsuccessful results')
            res.send({
                message: result.message
            })
        }
    })
    */
    
    Companies.findByIdAndUpdate(co_id, {$push: {jobs: newJob}})
        .exec()
        .then(company => {
            let jobsArr = company.jobs 
            jobsArr.push(newJob)
            res.send({
                JobsPosted: jobsArr
            })
        })
        .catch(error => {
            console.log(error)
            res.status(500).send({
                message: 'Failed to post new job'
            })
        })
        
    
    /*
    db.query(`INSERT INTO jobs (title, deadline, date, location, salary, description, category, co_id)
    VALUES ('${title}', '${deadline}', '${date}', '${location}', '${salary}', '${description}', '${category}', '${co_id}')`,
    (error, result) => {
        console.log('result from inserting (outer loop): ', result)
        if (error) throw error
        console.log('Posting New Job with co_id: ', co_id)
        db.query('SELECT * FROM jobs WHERE co_id = ?', [`${co_id}`], (error, results, fields) => {
            if (error) throw error
            console.log('results from selecting (inner loop): ', results)
            res.send({
                JobsPosted: results
            })
        })
        
    })
    */
})

//Company updating Status of Student's application 
router.post('/update-job-status', checkAuth, (req, res) => {

    let co_id = req.session.userId

    let {job_id, newStatus, student_id} = req.body
    
    /*
    let msg = {
        ...req.body,
        co_id,
        path: 'Update-Job-Status'
    }

    kafka.make_request('jobs', msg, (error, result) => {
        if(error){
            console.log('error')
            res.send({
                message: 'Error in kafka request'
            })
        } else if (result.success){
            console.log('Result: ', result.JobsApplied)
            res.send({
                JobsApplied: result.JobsApplied
            })
        } else if (!result.success){
            console.log('Unsuccessful results')
            res.send({
                message: result.message
            })
        }
    })
    */
    
    Students.findOneAndUpdate({
        _id: student_id, "jobs.job_id": job_id, "jobs.co_id": co_id
    }, 
    {
        $set: {
            "jobs.$.status": newStatus
        }
    })
    .exec()
    .then(response => {
        console.log('test response: ', response)
        Students.find({
            jobs: {
                $elemMatch: {co_id: co_id}
            }
        }).exec()
            .then(students => {
                let jobsArr = []
                students.forEach(student => student.jobs.forEach(job => {
                    if (job.co_id === co_id) jobsArr.push(job)
                }))
                console.log('Company Jobs Applied: ', jobsArr)
                res.send({
                    JobsApplied: jobsArr
                })
            })
            .catch(error => {
                console.log(error)
                res.status(500).send({
                    message: 'Failed to find applied Jobs'
                })
            })
    })
    .catch(error => {
        console.log(error)
        res.status(500).send({
            message: 'Failed to update job status'
        })
    })
    
})

//Get Job List for Student
router.get('/get-job-list', checkAuth, (req, res) => {
    //console.log('headers', req.headers)

   // console.log('session id check after logging in as student: ', req.session.userId)
    /*
    let msg = {
        path: 'Get-Job-List'
    }
    
    kafka.make_request('jobs', msg, (error, result) => {
        if(error){
            console.log('error')
            res.send({
                message: 'Error in kafka request'
            })
        } else if (result.success){
            console.log('Result: ', result.Jobs)
            res.send({
                Jobs: result.Jobs
            })
        } else if (!result.success){
            console.log('Unsuccessful results')
            res.send({
                message: result.message
            })
        }
    })
    */
    
    Companies.find()
        .exec()
        .then(companies => {
            const JobsArr = []
            companies.forEach(company => company.jobs.forEach(job => JobsArr.push(job)))
            res.send({
                Jobs: JobsArr
            })

        })
        .catch(error => {
            console.log(error)
            res.status(500).send({
                message: 'Unable to get Job List'
            })
        })
        
        
    /*
    db.query('SELECT * FROM jobs', (error, results, fields) => {
        if (error) throw error 
        res.send({
            Jobs: results
        })
    })
    */
})

module.exports = {
    jobPostsApiRouter: router
};
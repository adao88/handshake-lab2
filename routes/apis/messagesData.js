const express = require('express')
const {db} = require('../../db')
const bodyParser = require('body-parser');
const router = express.Router();
const app = express()
const passport = require('passport')
const checkAuth = passport.authenticate("jwt", { session: false });

const Students = require('../../Models/StudentModel')
const Companies = require('../../Models/CompanyModel')
const Messages = require('../../Models/MessageModel')

//const kafka = require('../kafka/client')


app.use(bodyParser.json());

router.get('/get-conversations', checkAuth, (req, res) => {

    let id = req.session.userId
    /*
    let msg = {
        id,
        path: 'Get-Conversations'
    }

    kafka.make_request('messages', msg, (err, result) => {
        if(err){
            console.log(error)
            res.status(500).send({
                message: 'Error in Kafka Request'
            })
        } else if(result.success){
            res.status(200).send({
                conversations: result.conversations
            })
        } else if (!result.success){
            res.status(500).send({
                message: result.message
            })
        }
    })
    */
    
    let conversations = []

    Messages.find({        
        $or: [
            {user1_id: id},
            {user2_id: id}
        ]
    })
    .exec()
    .then(results => {
        console.log('User\'s conversations: ', results)
        conversations = results
        console.log('conversations: ', conversations)
        res.send({
            conversations
        })
    })
    .catch(error => {
        console.log(error)
        res.send({
            message: 'Error in finding conversations'
        })
    })

    
})

router.post('/open-conversation', checkAuth, (req, res) => {

    let {chat_id} = req.body

    /*
    let msg = {
        chat_id,
        path: 'Open-Conversation'
    }

    kafka.make_request('messages', msg, (err, result) => {
        if(err){
            console.log(error)
            res.status(500).send({
                message: 'Error in Kafka Request'
            })
        } else if(result.success){
            res.status(200).send({
                conversation: result.conversation
            })
        } else if (!result.success){
            res.status(500).send({
                message: result.message
            })
        }
    })
    */
    
    Messages.findById(chat_id)
    .exec()
    .then(conversation => {
        if(!conversation){
            res.status(500).send({
                message: 'Conversation Not Found'
            })
        }
        console.log('conversation: ', conversation)
        res.status(200).send({
            conversation
        })
    })
    .catch(error => {
        console.log(error)
        res.status(500).send({
            message: 'Error in opening conversation'
        })
    })
    
})

router.post('/send-message', checkAuth, (req,res) => {

    
    let {chat_id, text} = req.body

    let user = req.session.name 
    
    /*
    let msg = {
        chat_id,
        text,
        user,
        path: 'Send-Message'
    }

    kafka.make_request('messages', msg, (err, result) => {
        if(err){
            console.log(error)
            res.status(500).send({
                message: 'Error in Kafka Request'
            })
        } else if(result.success){
            res.status(200).send({
                conversation: result.conversation
            })
        } else if (!result.success){
            res.status(500).send({
                message: result.message
            })
        }
    })
    */
    

    let textObj = {
        text,
        user
    }
    console.log('message text obj: ', textObj)

    Messages.findOneAndUpdate({
        _id: chat_id
    }, {
        $push: {
            messages: textObj
        }
    }, {
        new: true
    })
    .exec()
    .then(conversation => {
        if(!conversation){
            res.status(204).send({
                message: 'Could Not Find Conversation'
            })
        }
        console.log('Updated Conversation Obj: ', conversation)
        res.send({
            conversation
        })
    })
    .catch(error => {
        console.log(error)
        res.status(500).send({
            message: 'Failed to send message'
        })
    })
    
})

router.post('/start-conversation', async (req, res) => {

    let id = req.session.userId
    let user_name = req.session.name
    let recipient_id = req.body.user_id
    let recipient_name
    /*
    let msg = {
        id,
        user_name,
        recipient_id,
        path: 'Start-Conversation'
    }

    kafka.make_request('messages', msg, (err, result) => {
        if(err){
            console.log(error)
            res.status(500).send({
                message: 'Error in Kafka Request'
            })
        } else if(result.success){
            res.status(200).send({
                dat: result.data
            })
        } else if (!result.success){
            res.status(500).send({
                message: result.message
            })
        }
    })
    */
    

    await Students.findById(recipient_id)
        .exec()
        .then(student => {
            if(student) {
                recipient_name = student.name
            }
        })
    await Companies.findById(recipient_id)
        .exec()
        .then(company => {
            if (company){
                recipient_name = company.name
            }
        })

        console.log('Current User name: ', user_name)
        console.log('Recipient Name: ', recipient_name)

    let newChat = new Messages ({
            user1_id: id,
            user1_name: user_name,
            user2_id: recipient_id,
            user2_name: recipient_name,
            messages: [{
                user: user_name,
                text: 'Hello.'
            }]
        })

    Messages.findOne({
                $or: [{
                    $and: [
                        {user1_id: id},
                        {user2_id: recipient_id},
                    ]
                }, {
                    $and: [
                        {user1_id: recipient_id},
                        {user2_id: id}
                    ]
                }]  
    })
    .exec()
    .then(result => {
        console.log('Message query result: ', result)
        if(!result){
            newChat.save((error, data) => {
                if (error) console.log(error)
                console.log('Data after saving: ', data)
                res.status(200).send({
                    message: 'Able to start conversation',
                    data
                })
                res.end()
            })
        } else {
            res.status(200).send({
                message: 'Conversation already started'
            })
            res.end()
        }
    })
    .catch(error => {
        console.log(error)
        res.status(500).send({
            message: 'Unable to start conversation'
        })
        res.end()
    })
    
})


module.exports = {
    messagesApiRouter: router
}
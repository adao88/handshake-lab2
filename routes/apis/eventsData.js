const express = require('express')
//const {db} = require('../../db')
const bodyParser = require('body-parser');
const router = express.Router();
const app = express()
const passport = require('passport')
const checkAuth = passport.authenticate("jwt", { session: false });

const Students = require('../../Models/StudentModel')
const Companies = require('../../Models/CompanyModel')

//const kafka = require('../kafka/client')

app.use(bodyParser.json());

//gets all registered events for a company
router.get('/get-companyEvents', checkAuth,  (req, res) => {
    let co_id = req.session.userId

    /*
    let msg = {
        co_id,
        path: 'Get-Company-Registered-Events'
    }

    kafka.make_request('events', msg, (error, result) => {
        if (error){
            console.log(error)
            res.status(500).send({
                message: 'Error in kafka request'
            })
        } else if (result.success){
            res.send({
                events: result.events
            })
        } else if (!result.success){
            res.status(500).send({
                message: result.message
            })
        }

    })
    */
    
    Students.find(
        {
            events: {$elemMatch: {co_id: co_id}}
        })
        .exec()
        .then(students => {
            let eventsArr = []
            students.forEach(student => student.events.forEach(event => {
                if (event.co_id === co_id) eventsArr.push(event)
            }))
            console.log('Company Events Registered: ', eventsArr)
            res.send({
                events: eventsArr
            })
        })
        .catch(error => {
            console.log(error)
            res.status(500).send({
                message: 'Failed to get registered events'
            })
        })
    
    
    /*
    db.query('SELECT * FROM events_students WHERE co_id = ?', [`${co_id}`], (error, results, fields) => {
        if (error) throw error

        console.log('Events results: ', results)
        res.send({
            events: results
        })
    })
    */
})

//get  all events for a company
router.get('/get-all-company-events', checkAuth,  (req, res) => {
    let co_id = req.session.userId

    /*
    let msg = {
        co_id,
        path: 'Get-All-Company-Events'
    }

    kafka.make_request('events', msg, (error, result) => {
        if (error){
            console.log(error)
            res.status(500).send({
                message: 'Error in kafka request'
            })
        } else if (result.success){
            res.send({
                events: result.events
            })
        } else if (!result.success){
            res.status(500).send({
                message: result.message
            })
        }

    })
    */
    
    Companies.findById(co_id)
        .exec()
        .then(company => {
            console.log('Company Events: ', company.events)
            res.send({
                events: company.events
            })
        })
        .catch(error => {
            console.log(error)
            res.status(500).send({
                message: 'Failed to get Company Events'
            })
        })
    
    
    /*
    db.query('SELECT * FROM events_company WHERE co_id = ?', [`${co_id}`], (error, results, fields) => {
        if (error) throw error

        console.log('Events results: ', results)
        res.send({
            events: results
        })
    })
    */
})


//gets all events for a student
router.get('/get-all-events', checkAuth,  (req,res) => {
    /*
    let msg = {
        path: 'Get-All-Events'
    }

    kafka.make_request('events', msg, (error, result) => {
        if (error){
            console.log(error)
            res.status(500).send({
                message: 'Error in kafka request'
            })
        } else if (result.success){
            res.send({
                events: result.events
            })
        } else if (!result.success){
            res.status(500).send({
                message: result.message
            })
        }
    })
    */
    
    Companies.find()
        .exec()
        .then(companies => {
            let eventsArr = []
            companies.forEach(company => company.events.forEach(event => eventsArr.push(event)))
            res.send({
                events: eventsArr
            })
        })
        .catch(error => {
            console.log(error)
            res.status(500).send({
                message: 'Failed to get All Events'
            })
        })
    
        
    /*
    db.query('SELECT * FROM events_company', (error, results, fields) => {
        if (error) throw error 
        console.log('events results: ', results)
        res.send({
            events: results
        })
    })
    */

})


//get registered events for a user
router.get('/get-registered-events', checkAuth,  (req, res) => {
    let student_id = req.session.userId
    /*
    let msg = {
        student_id,
        path: 'Get-Student-Registered-Events'
    }

    kafka.make_request('events', msg, (error, result) => {
        if (error){
            console.log(error)
            res.status(500).send({
                message: 'Error in kafka request'
            })
        } else if (result.success){
            res.send({
                events: result.events
            })
        } else if (!result.success){
            res.status(500).send({
                message: result.message
            })
        }
    })
    */
    
    Students.findById(student_id).exec()
        .then(student => {
            console.log('Student\'s Registered Events: ', student.events)
            res.send({
                events: student.events
            })
        })
        .catch(error => {
            console.log(error)
            res.status(500).send({
                message: 'Failed to get student\'s registered events'
            })
        })
    
        
    /*
    db.query('SELECT * FROM events_students WHERE student_id = ?', [`${student_id}`], (error, results, fields) => {
        if (error) throw error
        console.log('registered events results: ', results)
        res.send({
            events: results
        })
    })
    */
})

//apply to event, but checks eligibility first
router.post('/apply-to-event', checkAuth, async (req, res) => {
    let student_id = req.session.userId
    let student_name = req.session.studentName
    let student_degree = req.session.degree
    
    let {
        co_name,
        eligibility,
        event_id,
        event_name,
        co_id,
        date,
        time,
        location
    } = req.body

    let event = {
        co_id,
        event_name,
        date,
        time,
        co_name,
        location,
        event_id,
        location,
        student_name,
        student_id
    }
    
    
    console.log('degree: ', student_degree)
    console.log('req body: ', req.body)
    /*
    let msg = {
        ...req.body,
        student_id,
        student_name,
        student_degree,
        path: 'Apply-To-Event'
    }
    
    kafka.make_request('events', msg, (error, result) => {
        if (error) {
            console.log(error)
            res.status(500).send({
                message: 'Error in Kafka Request'
            })
        }

        res.send({
            message: result.message
        })
    })
    */
    
    //let alreadyRegisteredResults
    let studentEligible = false

    if(!student_degree) {
        student_degree = ""
    }

    if (!eligibility || eligibility === 'All' || eligibility === 'all'){
        eligibility = student_degree.toLowerCase()
    }
    //check eligibility
    if(eligibility.toLowerCase() !== student_degree.toLowerCase()){
        console.log('eligibility do not match')
        return res.send({
            message: "Sorry, you are not eligibile for this event"
        })
        
    } else {
        studentEligible = true
    }

    //check if already registered
    Students.find(
        {
            _id: student_id, events: {$elemMatch: {event_id: event_id} }
        })
        .exec()
        .then(results => {
            console.log('Apply to event results: ', results)
            if (results.length === 0) {
                Students.findByIdAndUpdate(student_id,
                    {
                        $push: {events: event}
                    })
                    .exec()
                    .then(result => {
                        return res.send({
                            message: 'Registered for event'
                        })
                    })
                    .catch(error => {
                        console.log(error)
                        res.status(500).send({
                            message: 'Unable to register for event'
                        })
                    })
            }
            else {
                return res.send({
                    message: 'You\'ve already registered for this'
                })
                
            }
        })
        .catch(error => {
            console.log(error)
            res.status(500).send({
                message: 'Failed to check if already registered for event'
            })
        })

        //Student is not registered and degree eligible
        /*
        if(!registered && studentEligible){
            Students.findByIdAndUpdate(student_id,
                {
                    $push: {events: event}
                })
                .exec()
                .then(result => {
                    res.send({
                        message: 'Registered for event'
                    })
                })
                .catch(error => {
                    console.log(error)
                    res.status(500).send({
                        message: 'Unable to register for event'
                    })
                })
        }
        */
        /*
    //check  eligibility
    db.query('SELECT * FROM events_company WHERE id = ?', 
    [`${event_id}`], (error, results, fields) => {
        console.log('event application query result: ', results)

        if(eligibility.toLowerCase() !== student_degree.toLowerCase()){
            console.log('eligibility do not match')
            res.send({
                message: "Sorry, you are not eligibile for this event"
            })

        }

        //check if already registered
        db.query('SELECT * FROM events_students WHERE student_id = ? AND event_id =?', [`${student_id}`, `${event_id}`],
            async (error, results, fields) => {
                if (error) throw error

                alreadyRegisteredResults = await results
                console.log('reg check: ', alreadyRegisteredResults)

            if(alreadyRegisteredResults.length >= 1){

                res.send({
                    message: 'You\'ve already registered for this'
                })
            } else if(alreadyRegisteredResults.length === undefined || alreadyRegisteredResults.length < 1 ) {
                console.log('eligibility match each other')
                
                db.query(`INSERT INTO events_students (co_name, event_id, event_name, co_id, date, time, student_name, student_id) 
                VALUES('${co_name}', '${event_id}', '${event_name}', '${co_id}', '${date}', '${time}', '${student_name}', '${student_id}')`,
                    (error, result) => {
                        if (error) throw error
                        console.log('result of regestering: ', result)
                        res.send({
                            message: 'Registered for Event'
                        })
                    }
                )
            } 
        })
    })
    */

})

router.post('/create-event', checkAuth, (req,res) => {

    let co_id = req.session.userId
    let co_name = req.session.company_name
    
    let {name, 
        time,
        location,
        date,
        description,
        eligibility} = req.body

    let eventObj = {
        name, 
        time,
        location,
        date,
        description,
        eligibility,
        co_id,
        co_name
    }
    
    /*
    let msg = {
        ...req.body,
        co_id,
        co_name,
        path: 'Create-Event'
    }

    kafka.make_request('events', msg, (error, result) => {
        if (error){
            console.log(error)
            res.status(500).send({
                message: 'Error in kafka request'
            })
        } else if (result.success){
            res.send({
                event: result.event
            })
        } else if (!result.success){
            res.status(500).send({
                message: result.message
            })
        }
    })
    */
    
    Companies.findByIdAndUpdate(co_id,
        {
            $push: {events: eventObj}
        })
        .exec()
        .then(response => {
            res.send({
                message: "Event Created",
                event: eventObj
            })
        })
    
    /*
    db.query(`INSERT INTO events_company (co_id, name, time, location, date, description, eligibility)
    VALUES('${co_id}', '${name}', '${time}', '${location}', '${date}', '${description}', '${eligibility}')`,
    (error, result) => {
        if (error) throw error
        res.send({
            message: "Event Created",
            event: eventObj 
        })
    })
    */
})

module.exports = {
    eventsApiRouter: router
}
const express = require('express')
//const {db} = require('../../db')
const bodyParser = require('body-parser');
const router = express.Router();
const app = express()

const passport = require('passport')
const checkAuth = passport.authenticate("jwt", { session: false });

const Students = require('../../Models/StudentModel')

//const kafka = require('../kafka/client')


app.use(bodyParser.json());

//Route for Student to apply to a job
router.post('/apply-to-job', checkAuth, (req, res) => {

    let {co_id, title, job_id, company} = req.body
    let student_id = req.session.userId
    let student_name = req.session.studentName
    
    let today = new Date().toLocaleDateString()
    console.log('company name: ', company)
    let newJob = {
        co_id,
        title,
        job_id,
        company,
        student_name,
        student_id,
        date: today,
        status: 'Pending'
    }
/*
    let msg = {
        ...req.body,
        student_id,
        student_name,
        path: 'Apply-To-Job'
    }

    kafka.make_request('applications', msg, (error, result) => {
        if(error){
            res.send( {
                message: 'Error in Kafka Request'
            })
        } else if (result){
            res.send({
                message: result.message
            })
        } else {
            res.send({
                message: 'Some other Error in application submission'
            })
        }
    })
*/
    //console.log('Application: ', newJob)
    
    Students.findOne(
        {
            _id: student_id
        }
    ).exec()
    .then(student => {
        console.log('Found Student: ', student)
        if (student.jobs.some(job => job.job_id === job_id)){
            console.log('Found existing job')
            res.send({
                message: "You've already applied"
            })
        } else {
            console.log('didn\'t find existing job')
            Students.findByIdAndUpdate(student_id,
                {
                    $push: {jobs: newJob}
                }).exec()
                .then(response => {
                    console.log('Job Application submitted')
                    res.send({
                        message: "Application Submitted"
                    })
                })
                .catch(error => console.log(error))
        }
    })
    .catch(error => console.log(error))
    

    /*
    db.query('SELECT * FROM jobs_students WHERE job_id = ? AND student_id = ?', 
    [`${job_id}`, `${student_id}`], (error, results, fields) => {
        if (error) throw error
        console.log('outer loop results: ', results)
        //console.log('results: ', results)
        if(results.length > 0) {
            res.send({
                message: 'You have already applied to this job'
            })
        } else {
            db.query(`INSERT INTO jobs_students (student_id, co_id, status, student_name, title, job_id, company) 
            VALUES('${student_id}', '${co_id}', 'Pending', '${student_name}', '${title}', '${job_id}', '${company}')`,
            (error, result) => {
                if(error) throw error 
                console.log('result inner loop:')
                res.send({
                    message: "Application Submitted"
                })
            })
        }
    })
    */
})

//get Students' jobs applied to
router.get('/get-applications', checkAuth, (req, res) =>{

        let student_id = req.session.userId
        console.log('student_id: ', student_id)
    /*
        let msg = {
            student_id,
            path: 'Get-Applications'
        }

        kafka.make_request('applications', msg, (error, result) => {
            if(error){
                res.status(500).send({
                    message: 'Error in Kafka Request'
                })
            } else if(result.success) {
                res.send({
                    apps: result.apps
                })
            } else  if(!result.success){
                res.status(500).send({
                    message: result.message
                })
            }
        })
        */
        
        Students.findById(student_id).exec()
            .then(student => {
                console.log('Student Applications:', student.jobs)
                res.send({
                    apps: student.jobs
                })
            })
            .catch(error => {
                console.log(error)
                res.status(500).send({
                    message: 'Failed to get Student applications'
                })
            })

        /*
        db.query('SELECT * FROM jobs_students WHERE student_id = ?',
        [`${student_id}`], (error, results, fields) => {
            if (error) throw error
            console.log('results: ', results)

            res.send({
                apps: results
            })
        })
        */
})

module.exports = {
    applicationsApiRouter: router
}
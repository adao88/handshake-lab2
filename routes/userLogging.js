const bcrypt = require('bcryptjs')
const express = require('express')
//const {db} = require('../db')
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken')
const Students = require('../Models/StudentModel')
const Companies = require('../Models/CompanyModel')



const router = express.Router();
const app = express()

const { auth } = require('../Utils/passport')
const {secret} = require('../Utils/config')

//const kafka = require('./kafka/client')


app.use(bodyParser.json());

auth();


router.post('/register', (req, res) => {
    console.log('req is', req.body)

    //Create new Student (schema)HERE!!!
    /*
    let msg = req.body
    
    msg.path = 'Student-Register'



    kafka.make_request('account', msg, async (err, results) => {
        let kafkaResults = await results
        
        console.log('In Kafka make request registration account topic')
        console.log('registration results: ', results)
        if (err){
            console.log("Inside err");
            res.json({
                status:"error",
                msg:"System Error, Try Again."
            })
        }else{
            console.log("Inside else");
                res.json({
                    results : kafkaResults
                });

                res.end();
            }

    })
    */
    let {email, password, name, school} = req.body

    let newStudent = new Students({
        name,
        email,
        email_profile: email,
        school,
        password: ''
    })
    
    Students.findOne({email: email}, (error, student) => {
        console.log('In mongo query')
        if (error) {
            res.writeHead(500, {
                'Content-Type': 'text/plain'
            })
            res.end()
        }
        if (student){
            res.writeHead(400, {
                'Content-Type': 'text/plain'
            })
            console.log("user already exisists")
            res.end("User already exists")
            
        } else {
            bcrypt.hash(password, 10, (err, hash) => {
                if (err) throw err
                newStudent.password = hash
                newStudent.save((error, data)=> {
                    if (error) {
                        res.writeHead(500, {
                            'Content-Type': 'text/plain'
                        })
                        res.end
                    } else {
                        console.log('registration success')
                        res.status(200).send({
                            registration: true 
                        })
                    }
                })
            })
        }
    })
    
})

router.post('/company-register', (req, res) => {
    console.log('req is: ', req.body)

    /*
    let msg = req.body 
    msg.path = 'Company-Register'
    

    kafka.make_request('account', msg, async (err, results) => {
        let kafkaResults = await results
        
        console.log('In Kafka make request registration account topic')
        console.log('registration results: ', results)
        if (err){
            console.log("Inside err");
            res.json({
                status:"error",
                msg:"System Error, Try Again."
            })
        }else{
            console.log("Inside else");
                res.json({
                    results : kafkaResults
                });

                res.end();
            }

    })
    */

    let email = req.body.email
    let password = req.body.password
    let name = req.body.name
    
    let msg = req.body 
    msg.path = 'Company-Register'
    
    let newCompany = new Companies({
        name: name,
        email: email,
        email_profile: email,
        password: ''
    })
    
    Companies.findOne({email: email}, (error, company) => {
        console.log('In mongo query')
        if (error) {
            res.writeHead(500, {
                'Content-Type': 'text/plain'
            })
            res.end()
        }
        if (company){
            res.writeHead(400, {
                'Content-Type': 'text/plain'
            })
            console.log("user already exisists")
            res.end("User already exists")
            
        } else {
            bcrypt.hash(password, 10, (err, hash) => {
                if (err) throw err
                newCompany.password = hash
                newCompany.save((error, data)=> {
                    if (error) {
                        res.writeHead(500, {
                            'Content-Type': 'text/plain'
                        })
                        res.end
                    } else {
                        console.log('registration success')
                        res.status(200).send({
                            registration: true 
                        })
                    }
                })
            })
        }
    })
    
})

router.post('/login', (req, res) => {
    
    let email = req.body.email
    let password = req.body.password
    
    /*
    let msg = req.body
    msg.path = 'Student-Login'
    console.log('MSG to be sent: ', msg)

    kafka.make_request('account', msg , function(err,results){
        console.log('in result of login test api');
        console.log('login test results: ', results);
        if (err){
            console.log("Inside err");
            res.send({
                status:"error",
                msg:"System Error, Try Again."
            })
        } else if(results.loginSuccess) {
            const token = results.token
        
            req.session.userId = results.studentID
            req.session.studentName = results.student_name
            req.session.degree = results.student_degree
            req.session.role = 'student' 
            req.session.name = results.student_name

            res.cookie('Student-Logged',"user", {maxAge: 900000, httpOnly: false, path : '/'})
            res.cookie('token', token, {maxAge: 900000, httpOnly: false, path : '/'})
            res.cookie('Name', `${req.session.name}`, {maxAge: 900000, httpOnly: false, path : '/'})
            console.log('id check: ', req.session.userId)
            
            res.send({
                results : results
            });
        } else {
            res.send({
                message: results.message
            })
        }
        
    })
    */
    
    Students.findOne({email: email}, (err, student) => {
        if (err) {
            res.status(500).send({
                message: "Error Occured"
            })
        }
        if (student) {
            console.log('student: ', student)
            bcrypt.compare(password, student.password, (err, result) => {
                console.log('Bcrypt result: ', result)
                if (result){
                    
                    const payload = { _id: student._id, name: student.name, role: 'student'};
                    const token = jwt.sign(payload, secret, {
                        expiresIn: 1008000
                    })
                    console.log('token: ', token)
                    console.log('document result: ', student)

                    req.session.userId = student._id
                    req.session.studentName = student.name
                    req.session.degree = student.degree
                    req.session.name = student.name

                    const authToken = `jwt ${token}`
                    console.log('authToken: ', authToken)
                    console.log(req.session.userId)
                    res.cookie('Student-Logged',"user", {maxAge: 900000, httpOnly: false, path : '/'})
                    res.cookie('token', authToken, {maxAge: 900000, httpOnly: false, path : '/'})
                    res.cookie('Name', req.session.name, {maxAge: 900000, httpOnly: false, path : '/'})
                    res.send({
                        login: 'Success',
                        message: `Session with ${email} with ${req.session.userId}`,
                        id: req.session.userId,
                        token: token
                    })
                } else {
                    res.send({
                        login: 'Failed',
                        message: 'Login Failed. Incorrect Password'
                    })
                }
            })
        } else {
            res.send({
                login: 'Failed',
                message: 'Login Failed. Incorrect email'
            })
        }
    })
    
})

router.post('/company-login',  (req, res) => {
    
    let email = req.body.email
    let password = req.body.password
    
    /*
    let msg = req.body 
    msg.path = 'Company-Login'

    kafka.make_request('account' , msg , function(err,results){
        console.log('in result of login test api');
        console.log('login test results: ', results);
        if (err){
            console.log("Inside err");
            res.send({
                status:"error",
                msg:"System Error, Try Again."
            })
        } else if(results.loginSuccess) {
            const token = results.token

            res.cookie('Company-Logged',"Company", {maxAge: 900000, httpOnly: false, path : '/'})
            res.cookie('token', token, {maxAge: 900000, httpOnly: false, path : '/'})
                    
            req.session.userId = results.companyID
            req.session.companyName = results.company_name
            req.session.role = 'company'
            req.session.name = results.company_name

            res.cookie('Name', `${req.session.name}`, {maxAge: 900000, httpOnly: false, path : '/'})
            

            console.log('id check: ', req.session.userId)
            
            res.send({
                results : results
            });
        } else {
            res.send({
                message: results.message
            })
        }
        
    })
    */
    
    Companies.findOne({email: email}, (err, company) => {
        if (err) {
            res.status(500).send({
                message: "Error Occured"
            })
        }
        if (company) {
            console.log('company: ', company)
            bcrypt.compare(password, company.password, (err, result) => {
                console.log('Bcrypt result: ', result)
                if (result){
                    
                    const payload = { _id: company._id, name: company.name, role: 'company'};
                    const token = jwt.sign(payload, secret, {
                        expiresIn: 1008000
                    })
                    console.log('token: ', token)
                    console.log('document result: ', company)

                    req.session.userId = company._id
                    req.session.company_name = company.name
                    req.session.name = company.name
                    
                    const authToken = `jwt ${token}`
                    console.log(req.session.userId)
                    res.cookie('Company-Logged',"Company", {maxAge: 900000, httpOnly: false, path : '/'})
                    res.cookie('token', authToken, {maxAge: 900000, httpOnly: false, path : '/'})
                    res.cookie('Name', req.session.name, {maxAge: 900000, httpOnly: false, path : '/'})
                    res.send({
                        login: 'Success',
                        message: `Session with ${email} with ${req.session.userId}`,
                        id: req.session.userId,
                        token: token
                    })
                } else {
                    res.send({
                        login: 'Failed',
                        message: 'Login Failed. Incorrect Password'
                    })
                }
            })
        } else {
            res.send({
                login: 'Failed',
                message: 'Login Failed. Incorrect Email'
            })
        }
    })
    

})

router.post('/logout', (req, res) => {
    res.clearCookie('cookie')
    res.clearCookie('Student-Logged')
    res.clearCookie('Company-Logged')
    res.clearCookie('token')
    res.clearCookie('Name')
    let id = req.session.userId
    res.clearCookie('connect.sid')
    req.session.destroy((err) =>{
        res.send({
            message: "logging out",
            sessionCheck: id
        })
    })
})

module.exports = {
    userRouter: router
}



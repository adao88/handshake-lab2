const mongoose = require('mongoose')
const Schema = mongoose.Schema



const conversationSchema = new Schema ({
    user1_id: String,
    user1_name: String,
    user2_id: String,
    user2_name: String,
    messages: [{
        user: String,
        text: String
    }]   
})

/*
const messageSchema = new Schema ({
    conversations: [conversationSchema]
})
*/
const messageModel = mongoose.model('messages', conversationSchema)
module.exports = messageModel
const mongoose = require('mongoose')
const Schema = mongoose.Schema



const chatSchema = new Schema ({
    user_id: String,
    user: String,
    messages: [{
        user: String,
        text: String
    }]   
})



const educationSchema = new Schema({
    schoolname: String,
    location: String,
    dates: String,
    gpa: String,
    degree: String
})

const experienceSchema = new Schema({
    company: String, 
    title: String,
    location: String,
    dates: String,
    description: String,
})

//Jobs applied to (Applications)
const jobSchema = new Schema({
    co_id: String,
    status: String,
    title: String,
    company: String,
    date: Date,
    job_id: String,
    student_name: String,
    student_id: String
})

const eventSchema = new Schema({
    co_id: String,
    event_name: String,
    date: Date,
    time: String,
    co_name: String,
    location: String,
    event_id: String,
    student_name: String,
    student_id: String,
})

const studentSchema = new Schema ({
    email: {type: String},
    email_profile: String,
    password: {type: String},
    name: {type: String},
    birthdate: String,
    location:  String,
    objective: String,
    phone: String,
    skillset: String,
    schoolname: String,
    degree: String,
    education: [educationSchema],
    experience: [experienceSchema],
    jobs: [jobSchema],
    events:[eventSchema],
    /*
    messages: [{
        _id: String,
        user_id: String,
        user_name: String,
        messages: [{
            user: String,
            text: String
        }] 
    }]
    */
})

const studentModel = mongoose.model('student', studentSchema)
module.exports = studentModel
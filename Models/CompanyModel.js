const mongoose = require('mongoose')
const Schema = mongoose.Schema


const chatSchema = new Schema ({
    user_id: String,
    user: String,
    messages: [{
        user: String,
        text: String
    }]   
})


const jobSchema = new Schema({
    title: String,
    date: Date,
    deadline: Date,
    location: String,
    salary: Number,
    category: String,
    description: String,
    co_id: String,
    co_name: String
})

const eventSchema = new Schema({
    name: String,
    time: String,
    location: String,
    date: Date,
    description: String,
    eligibility: String,
    co_id: String,
    co_name: String
})

const companySchema = new Schema({
    name: String,
    email:  String,
    password: String,
    email_profile: String,
    location: String,
    description: String,
    phone: String,
    jobs: [jobSchema],
    events: [eventSchema]
})

const companyModel = mongoose.model('company', companySchema)
module.exports = companyModel
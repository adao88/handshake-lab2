const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const passport = require("passport");
const { secret } = require("./config");
const Students = require('../Models/StudentModel')
const Companies = require('../Models/CompanyModel')


const auth = () => {
    const opts = {
        jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme("jwt"),
        secretOrKey: secret
    }

    passport.use( 
        new JwtStrategy(opts, (jwt_payload, callback) => {
            
            const user_id = jwt_payload._id 
            const role= jwt_payload.role
            
            if(role === 'student'){

                
                Students.findById(user_id, (err, results) => {
                    if (err) {
                        console.log('some error')
                        return callback(err, false);
                    }
                    if (results) {
                        console.log('user found in db')
                        callback(null, results);
                    }
                    else {
                        console.log('user not found in db')
                        callback(null, false);
                    }
                })
            } else if (role === 'company'){
                
                Companies.findById(user_id, (err, results) => {
                    if (err) {
                        console.log('some error')
                        return callback(err, false);
                    }
                    if (results) {
                        console.log('user found in db')
                        callback(null, results);
                    }
                    else {
                        console.log('user not found in db')
                        callback(null, false);
                    }
                })
            }
        })
    )
}



exports.auth = auth
exports.checkAuth = passport.authenticate('jwt', {session: false})